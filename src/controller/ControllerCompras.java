package controller;

import java.util.ArrayList;

import DAO.DAOCompras;
import model.ModelCompras;
import relatorios.DAORelatorios;

public class ControllerCompras {

    private DAOCompras daoCompras = new DAOCompras();
    private DAORelatorios dAORelatorios = new DAORelatorios();

    public int salvarComprasController(ModelCompras pModelCompras){
        return this.daoCompras.salvarComprasDAO(pModelCompras);
    }

    public ModelCompras getComprasController(int pCodigo){
        return this.daoCompras.getComprasDAO(pCodigo);
    }

    public ArrayList<ModelCompras> getListaComprasController(){
        return this.daoCompras.getListaComprasDAO();
    }

    public boolean atualizarComprasController(ModelCompras pModelCompras){
        return this.daoCompras.atualizarComprasDAO(pModelCompras);
    }

    public boolean excluirComprasController(int pCodigo){
        return this.daoCompras.excluirComprasDAO(pCodigo);
    }

    public boolean gerarRelatorioCompraController(int pCodigo) {
        return this.dAORelatorios.gerarRelatorioCompraDAO(pCodigo);
    }
}