package controller;

import java.util.ArrayList;

import DAO.DAOPagamento;
import model.ModelPagamento;

public class ControllerPagamento {

	private DAOPagamento daoPagamento = new DAOPagamento();

	public int salvarPagamentoController(ModelPagamento pModelPagamento) {
		return this.daoPagamento.salvarPagamentoDAO(pModelPagamento);
    }

	public ModelPagamento getPagamentoController(int pCodigo) {
		return this.daoPagamento.getPagamentoDAO(pCodigo);
    }

	public ArrayList<ModelPagamento> getListaPagamentoController() {
		return this.daoPagamento.getListaPagamentoDAO();
    }

	public boolean atualizarPagamentoController(ModelPagamento pModelPagamento) {
		return this.daoPagamento.atualizarPagamentoDAO(pModelPagamento);
    }

	public boolean excluirPagamentoController(int pCodigo) {
		return this.daoPagamento.excluirPagamentoDAO(pCodigo);
    }
}