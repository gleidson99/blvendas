package controller;

import java.util.ArrayList;

import DAO.DAOCaixa;
import model.ModelCaixa;

public class ControllerCaixa {

    private DAOCaixa daoCaixa = new DAOCaixa();

    public int salvarCaixaController(ModelCaixa pModelCaixa){
        return this.daoCaixa.salvarCaixaDAO(pModelCaixa);
    }

    public ModelCaixa getCaixaController(int pCodigo){
        return this.daoCaixa.getCaixaDAO(pCodigo);
    }

    public ArrayList<ModelCaixa> getListaCaixaController(){
        return this.daoCaixa.getListaCaixaDAO();
    }

    public boolean atualizarCaixaController(ModelCaixa pModelCaixa){
        return this.daoCaixa.atualizarCaixaDAO(pModelCaixa);
    }

    public boolean excluirCaixaController(int pCodigo){
        return this.daoCaixa.excluirCaixaDAO(pCodigo);
    }

    public ModelCaixa verificarRetorarCaixaControler(int numeroCaixa) {
        return this.daoCaixa.verificarRetorarCaixaDAO(numeroCaixa);
    }

    public boolean atualizarCaixaPDVController(ModelCaixa pModelCaixa) {
        return this.daoCaixa.atualizarCaixaPDVDAO(pModelCaixa);
    }

    public ModelCaixa retorarCaixaControler(int numeroCaixa) {
        return this.daoCaixa.retorarCaixaDAO(numeroCaixa);
    }
}