package controller;

import java.util.ArrayList;

import DAO.DAOItensPedidoMesa;
import model.ModelItensPedidoMesa;

public class ControllerItensPedidoMesa {

    private DAOItensPedidoMesa daoItensPedidoMesa = new DAOItensPedidoMesa();

    public int salvarItensPedidoMesaController(ModelItensPedidoMesa pModelItensPedidoMesa){
        return this.daoItensPedidoMesa.salvarItensPedidoMesaDAO(pModelItensPedidoMesa);
    }

    public boolean salvarItensPedidoMesaController(ArrayList<ModelItensPedidoMesa> pListaModelItensPedidoMesas){
        return this.daoItensPedidoMesa.salvarItensPedidoMesaDAO(pListaModelItensPedidoMesas);
    }

    public ModelItensPedidoMesa getItensPedidoMesaController(int pCodigo){
        return this.daoItensPedidoMesa.getItensPedidoMesaDAO(pCodigo);
    }

    public ArrayList<ModelItensPedidoMesa> getListaItensPedidoMesaController(){
        return this.daoItensPedidoMesa.getListaItensPedidoMesaDAO();
    }

    public ArrayList<ModelItensPedidoMesa> getListaItensPedidoMesaCozinhaController(){
        return this.daoItensPedidoMesa.getListaItensPedidoMesaCozinhaDAO();
    }

    public ArrayList<ModelItensPedidoMesa> getListaItensPedidoMesaController(int pCodigoMesa){
        return this.daoItensPedidoMesa.getListaItensPedidoMesaDAO(pCodigoMesa);
    }
    
    public ArrayList<ModelItensPedidoMesa> getListaItensPedidoPorVenda(int codigoVenda){
        return this.daoItensPedidoMesa.getListaItensPedidoPorVenda(codigoVenda);
    }

    public boolean atualizarItensPedidoMesaController(ModelItensPedidoMesa pModelItensPedidoMesa){
        return this.daoItensPedidoMesa.atualizarItensPedidoMesaDAO(pModelItensPedidoMesa);
    }

    public boolean atualizarStatusItemController(ModelItensPedidoMesa pModelItensPedidoMesa){
        return this.daoItensPedidoMesa.atualizarStatusItemDAO(pModelItensPedidoMesa);
    }

    public boolean excluirItensPedidoMesaController(int pCodigo){
        return this.daoItensPedidoMesa.excluirItensPedidoMesaDAO(pCodigo);
    }

    public ArrayList<Integer> getListaMesasOcupadasController() {
        return this.daoItensPedidoMesa.getListaMesasOcupadasDAO();
    }
}