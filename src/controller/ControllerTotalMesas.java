package controller;

import java.util.ArrayList;

import DAO.DAOTotalMesas;
import model.ModelTotalMesas;

public class ControllerTotalMesas {

    private DAOTotalMesas daoTotalMesas = new DAOTotalMesas();

    public int salvarTotalMesasController(ModelTotalMesas pModelTotalMesas){
        return this.daoTotalMesas.salvarTotalMesasDAO(pModelTotalMesas);
    }

    public ModelTotalMesas getTotalMesasController(int pCodigo){
        return this.daoTotalMesas.getTotalMesasDAO(pCodigo);
    }

    public ArrayList<ModelTotalMesas> getListaTotalMesasController(){
        return this.daoTotalMesas.getListaTotalMesasDAO();
    }

    public boolean atualizarTotalMesasController(ModelTotalMesas pModelTotalMesas){
        return this.daoTotalMesas.atualizarTotalMesasDAO(pModelTotalMesas);
    }

    public boolean excluirTotalMesasController(int pCodigo){
        return this.daoTotalMesas.excluirTotalMesasDAO(pCodigo);
    }
}