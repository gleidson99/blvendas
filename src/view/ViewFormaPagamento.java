package view;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import controller.ControllerFormaPagamento;
import model.ModelFormaPagamento;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;

public class ViewFormaPagamento extends javax.swing.JFrame {

    ControllerFormaPagamento controllerFormaPagamento = new ControllerFormaPagamento();
    ModelFormaPagamento modelFormaPagamento = new ModelFormaPagamento();
    ArrayList<ModelFormaPagamento> listaFormaPagamentos = new ArrayList<>();
    String tipoCadastro;

    public ViewFormaPagamento() {
        initComponents();
        setLocationRelativeTo(null);
        this.carregarFormaPagamento();
        this.limparCampos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        tfCodigo = new javax.swing.JTextField();
        tfDescricao = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btCancelar = new javax.swing.JButton();
        btSalvar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbFormaPagamento = new javax.swing.JTable();
        btAlterar = new javax.swing.JButton();
        btExcluir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        rbAtivo = new javax.swing.JRadioButton();
        rbInativo = new javax.swing.JRadioButton();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Forma de pagamento");
        setIconImage(new ImageIcon(getClass().getResource("/imagens/blicon.png")).getImage());
        setResizable(false);

        jLabel1.setText("Código:");

        tfCodigo.setEditable(false);
        tfCodigo.setEnabled(false);

        jLabel2.setText("Descrição:");

        btCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-fechar-janela-filled-50.png"))); // NOI18N
        btCancelar.setText("Cancelar");
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        btSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-salvar-filled-50.png"))); // NOI18N
        btSalvar.setText("Salvar");
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        tbFormaPagamento.setAutoCreateRowSorter(true);
        tbFormaPagamento.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "Código", "Descrição","Situação"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbFormaPagamento.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tbFormaPagamento.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbFormaPagamento.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tbFormaPagamento);
        if (tbFormaPagamento.getColumnModel().getColumnCount() > 0) {
            tbFormaPagamento.getColumnModel().getColumn(1).setMinWidth(300);
            tbFormaPagamento.getColumnModel().getColumn(1).setPreferredWidth(300);
            tbFormaPagamento.getColumnModel().getColumn(2).setMinWidth(110);
            tbFormaPagamento.getColumnModel().getColumn(2).setPreferredWidth(110);
            tbFormaPagamento.getColumnModel().getColumn(2).setMaxWidth(110);
        }

        btAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-editar-filled-50.png"))); // NOI18N
        btAlterar.setText("Alterar");
        btAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAlterarActionPerformed(evt);
            }
        });

        btExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-excluir-filled-50.png"))); // NOI18N
        btExcluir.setText("Excluir");
        btExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btExcluirActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Situação:"));

        buttonGroup1.add(rbAtivo);
        rbAtivo.setSelected(true);
        rbAtivo.setText("Ativo");

        buttonGroup1.add(rbInativo);
        rbInativo.setText("Inativo");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(rbAtivo)
                .addGap(32, 32, 32)
                .addComponent(rbInativo)
                .addContainerGap(45, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbAtivo)
                    .addComponent(rbInativo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(layout.createSequentialGroup()
        					.addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE))
        				.addGroup(layout.createSequentialGroup()
        					.addComponent(btCancelar)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(btExcluir)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(btAlterar)
        					.addPreferredGap(ComponentPlacement.RELATED, 296, Short.MAX_VALUE)
        					.addComponent(btSalvar))
        				.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
        					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
        						.addComponent(jPanel1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE)
        						.addGroup(layout.createSequentialGroup()
        							.addGroup(layout.createParallelGroup(Alignment.TRAILING)
        								.addComponent(tfCodigo, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
        								.addComponent(jLabel1, Alignment.LEADING))
        							.addPreferredGap(ComponentPlacement.RELATED)
        							.addGroup(layout.createParallelGroup(Alignment.LEADING)
        								.addComponent(jLabel2)
        								.addComponent(tfDescricao, GroupLayout.DEFAULT_SIZE, 567, Short.MAX_VALUE))))
        					.addGap(6)))
        			.addContainerGap())
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jLabel1)
        				.addComponent(jLabel2))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(tfCodigo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(tfDescricao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addGap(18)
        			.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(layout.createSequentialGroup()
        					.addGap(45)
        					.addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
        					.addGap(273))
        				.addGroup(layout.createSequentialGroup()
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
        					.addPreferredGap(ComponentPlacement.RELATED)))
        			.addGroup(layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(btCancelar)
        				.addComponent(btSalvar)
        				.addComponent(btAlterar)
        				.addComponent(btExcluir))
        			.addContainerGap())
        );
        getContentPane().setLayout(layout);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        if (tipoCadastro.equals("novo")) {
            salvarFormaPagamento();
            limparCampos();
            this.tfCodigo.setText("");
        } else if (tipoCadastro.equals("alteracao")) {
            alterarFormaPagamento();
            limparCampos();
            this.tfCodigo.setText("");
        }
    }//GEN-LAST:event_btSalvarActionPerformed

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        limparCampos();
    }//GEN-LAST:event_btCancelarActionPerformed

    private void btAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAlterarActionPerformed
        int linhaSelecionada = tbFormaPagamento.getSelectedRow();
        // Verificamos se existe realmente alguma linha selecionada
        if (linhaSelecionada < 0) {
            JOptionPane.showMessageDialog(this, "Você deve selecionar um item na tabela antes de clicar no botão!", "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
        } else {
            limparCampos();
            tipoCadastro = "alteracao";
            retornarFormaPagamento();
        }
    }//GEN-LAST:event_btAlterarActionPerformed

    private void btExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btExcluirActionPerformed
        int linhaSelecionada = tbFormaPagamento.getSelectedRow();
        //Verificamos se existe realmente alguma linha selecionada
        if (linhaSelecionada < 0) {
            JOptionPane.showMessageDialog(this, "Você deve selecionar um item na tabela antes de clicar no botão!", "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
        } else {
            this.excluirFormapagamento();
        }
    }//GEN-LAST:event_btExcluirActionPerformed

    private boolean alterarFormaPagamento() {
        modelFormaPagamento.setCodigo(Integer.parseInt(this.tfCodigo.getText()));
        modelFormaPagamento.setDescricao(this.tfDescricao.getText());
        if (rbAtivo.isSelected()) {
            modelFormaPagamento.setSituacao(true);
        } else {
            modelFormaPagamento.setSituacao(false);
        }

        //salvar 
        if (controllerFormaPagamento.atualizarFormaPagamentoController(modelFormaPagamento)) {
            JOptionPane.showMessageDialog(this, "Registro gravado com sucesso!");
            this.limparCampos();
            this.carregarFormaPagamento();
            return true;
        } else {
            JOptionPane.showMessageDialog(this, "Erro ao gravar os dados!", "ERRO", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    private boolean salvarFormaPagamento() {
        modelFormaPagamento.setDescricao(this.tfDescricao.getText());
        if (rbAtivo.isSelected()) {
            modelFormaPagamento.setSituacao(true);
        } else {
            modelFormaPagamento.setSituacao(false);
        }

        //salvar 
        if (controllerFormaPagamento.salvarFormaPagamentoController(modelFormaPagamento) > 0) {
            JOptionPane.showMessageDialog(this, "Registro gravado com sucesso!");
            this.limparCampos();
            this.carregarFormaPagamento();
            return true;
        } else {
            JOptionPane.showMessageDialog(this, "Erro ao gravar os dados!", "ERRO", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    private void excluirFormapagamento() {
        int linha = tbFormaPagamento.getSelectedRow();
        String nome = (String) tbFormaPagamento.getValueAt(linha, 1);
        int codigo = (Integer) tbFormaPagamento.getValueAt(linha, 0);
        //pegunta se realmente deseja excluir
        int opcao = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja"
                + " excluir o registro \nNome: "
                + nome + " ?", "Atenção", JOptionPane.YES_NO_OPTION);
        //se sim exclui, se não não faz nada    
        if (opcao == JOptionPane.OK_OPTION) {
            if (controllerFormaPagamento.excluirFormaPagamentoController(codigo)) {
                JOptionPane.showMessageDialog(this, "Registro excluido com suscesso!");
                carregarFormaPagamento();
            } else {
                JOptionPane.showMessageDialog(this, "Erro ao e os dados!", "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private boolean retornarFormaPagamento() {
        //recebe a linha selecionada
        int linha = this.tbFormaPagamento.getSelectedRow();
        //pega o codigo do cliente na linha selecionada
        int codigo = (Integer) tbFormaPagamento.getValueAt(linha, 0);
        try {
            //recupera os dados do banco
            modelFormaPagamento = controllerFormaPagamento.getFormaPagamentoController(codigo);
            //seta os dados na interface
            this.tfCodigo.setText(String.valueOf(modelFormaPagamento.getCodigo()));
            this.tfDescricao.setText(modelFormaPagamento.getDescricao());
            if (modelFormaPagamento.isSituacao() == true) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Código inválido ou nenhum registro selecionado", "Aviso", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    private void limparCampos() {
        tfCodigo.setText("Novo");
        tfDescricao.setText("");
        tipoCadastro = "novo";
    }

    private void carregarFormaPagamento() {
        listaFormaPagamentos = controllerFormaPagamento.getListaFormaPagamentoController();
        DefaultTableModel modelo = (DefaultTableModel) tbFormaPagamento.getModel();
        modelo.setNumRows(0);
        String situacao = "";
        //CARREGA OS DADOS DA LISTA NA TABELA
        int cont = listaFormaPagamentos.size();
        for (int i = 0; i < cont; i++) {
            if (listaFormaPagamentos.get(i).isSituacao() == false) {
                situacao = "Inativo";
            } else {
                situacao = "Ativo";
            }
            modelo.addRow(new Object[]{
                listaFormaPagamentos.get(i).getCodigo(),
                listaFormaPagamentos.get(i).getDescricao(),
                situacao
            });
        }
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewFormaPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewFormaPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewFormaPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewFormaPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewFormaPagamento().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAlterar;
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btExcluir;
    private javax.swing.JButton btSalvar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rbAtivo;
    private javax.swing.JRadioButton rbInativo;
    private javax.swing.JTable tbFormaPagamento;
    private javax.swing.JTextField tfCodigo;
    private javax.swing.JTextField tfDescricao;
    // End of variables declaration//GEN-END:variables
}
