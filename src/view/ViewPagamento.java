package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import controller.ControllerFormaPagamento;
import controller.ControllerPagamento;
import controller.ControllerPermissaousuario;
import model.ModelFormaPagamento;
import model.ModelPagamento;
import model.ModelSessaoUsuario;
import util.JMoneyField;

public class ViewPagamento extends JDialog {

	private BigDecimal valorTotal;
	private BigDecimal valorRecebido;
	private BigDecimal troco;
	private BigDecimal totalPagar;
	private BigDecimal valorCusto;
	private String tipoPagamento;
	private ViewVerificarPermissao viewVerificarPermissao;
	ModelFormaPagamento formaPagamento;

	ArrayList<ModelFormaPagamento> listaModelTipoPagamentos = new ArrayList<>();
	ControllerFormaPagamento controllerTipoPagamento = new ControllerFormaPagamento();
	JComboBox cbFormaPagamento = new JComboBox();
	private int codigoVenda;

	
	public static final int RET_CANCEL = 0;
	
	public static final int RET_OK = 1;

	public ViewPagamento(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
		setLocationRelativeTo(null);
		listarFormaPagamento();
		this.viewVerificarPermissao = new ViewVerificarPermissao(null, true);

		// Close the dialog when Esc is pressed
		String cancelName = "cancel";
		InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
		ActionMap actionMap = getRootPane().getActionMap();
		actionMap.put(cancelName, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				doClose(RET_CANCEL);
			}
		});
	}

	private static final String COMMIT_ACTION = "commit";

	private void listarFormaPagamento() {
		listaModelTipoPagamentos = controllerTipoPagamento.getListaFormaPagamentoController();
		cbFormaPagamento.removeAllItems();
		for (int i = 0; i < listaModelTipoPagamentos.size(); i++) {
			cbFormaPagamento.addItem(listaModelTipoPagamentos.get(i));
		}
		// JTextField mainTextField = new JTextField();
		// autocomplete.setFocusTraversalKeysEnabled(false);
		// List<String> keywords = new ArrayList<String>(5);
		// keywords.add("example");
		// keywords.add("autocomplete");
		// keywords.add("stackabuse");
		// keywords.add("java");
		// Autocomplete autoComplete = new Autocomplete(autocomplete, keywords);
		// autocomplete.getDocument().addDocumentListener(autoComplete);
		// autocomplete.getInputMap().put(KeyStroke.getKeyStroke("TAB"), COMMIT_ACTION);
		// autocomplete.getActionMap().put(COMMIT_ACTION, autoComplete.new CommitAction());
	}

	public int getReturnStatus() {
		return returnStatus;
	}

	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated
	// Code">//GEN-BEGIN:initComponents
	private void initComponents() {
		okButton = new JButton();
		jPanel1 = new JPanel();
		jlRS = new JLabel();
		jlfValorPagar = new JLabel();
		jLabel6 = new JLabel();
		jtfTroco = new JTextField();
		jtfTroco.setHorizontalAlignment(SwingConstants.CENTER);
		jPanel2 = new JPanel();
		jtfValorRecebido = new JMoneyField(9);
		jLabel4 = new JLabel();
		jLabel10 = new JLabel();
		jtfSubtotal = new JLabel();

		setTitle("Pagamento");
		setIconImage(new ImageIcon(getClass().getResource("/imagens/blicon.png")).getImage());
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				closeDialog(evt);
			}
		});

		okButton.setIcon(new ImageIcon(getClass().getResource("/imagens/icons 30/icons8-selecionado-filled-50.png"))); // NOI18N
		okButton.setText("OK");
		okButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				okButtonActionPerformed(evt);
			}
		});

		jPanel1.setBorder(BorderFactory.createTitledBorder(null, "Total a pagar", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 24))); // NOI18N

		jlRS.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
		jlRS.setForeground(new java.awt.Color(0, 153, 102));
		jlRS.setText("R$");

		jlfValorPagar.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
		jlfValorPagar.setForeground(new java.awt.Color(0, 153, 102));
		jlfValorPagar.setText("valor");

		GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addGap(136, 136, 136).addComponent(jlRS).addGap(56, 56, 56).addComponent(jlfValorPagar).addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addGap(22, 22, 22)
				.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jlRS).addComponent(jlfValorPagar)).addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
		jLabel6.setText("Troco:");

		jtfTroco.setEditable(false);
		jtfTroco.setBackground(new java.awt.Color(255, 204, 204));
		jtfTroco.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
		jtfTroco.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

			}
		});

		jPanel2.setBorder(BorderFactory.createTitledBorder("Forma de pagamento"));
		jtfValorRecebido.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
		jtfValorRecebido.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent evt) {
				jtfValorRecebidoFocusLost(evt);
			}
		});
		jtfValorRecebido.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				jtfValorRecebidoKeyReleased(evt);
			}
		});

		jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
		jLabel4.setText("Total recebido:");

		GroupLayout jPanel2Layout = new GroupLayout(jPanel2);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
						.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addGap(52).addComponent(jLabel4)).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap()
								.addComponent(cbFormaPagamento, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE).addGap(18).addComponent(jtfValorRecebido, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(74, Short.MAX_VALUE)));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup().addGap(21).addGroup(jPanel2Layout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(jtfValorRecebido, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(cbFormaPagamento, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE))
						.addGap(139).addComponent(jLabel4).addContainerGap(62, Short.MAX_VALUE)));
		jPanel2.setLayout(jPanel2Layout);

		jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
		jLabel10.setText("Valor total:");

		jtfSubtotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
		jtfSubtotal.setText("0.0");

		GroupLayout layout = new GroupLayout(getContentPane());
		layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addGroup(layout.createParallelGroup(Alignment.LEADING)
								.addGroup(layout.createParallelGroup(Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(30).addComponent(jLabel10).addPreferredGap(ComponentPlacement.UNRELATED).addComponent(jtfSubtotal))
										.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(layout.createSequentialGroup().addComponent(jLabel6).addPreferredGap(ComponentPlacement.RELATED).addComponent(jtfTroco, 192, 192, 192).addGap(18).addComponent(okButton, GroupLayout.PREFERRED_SIZE, 190,
										GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(Alignment.BASELINE).addComponent(jLabel10).addComponent(jtfSubtotal)).addGap(17)
						.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(18)
						.addGroup(layout.createParallelGroup(Alignment.TRAILING)
								.addGroup(layout.createParallelGroup(Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(13).addComponent(jLabel6)).addComponent(jtfTroco, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
								.addComponent(okButton, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		getContentPane().setLayout(layout);

		getRootPane().setDefaultButton(okButton);

		pack();
	}

	private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {
		// verifica se o valor recebido é o suficiente para pagar
		BigDecimal ptroco = BigDecimal.ZERO;
		try {
			ptroco = new BigDecimal(jtfTroco.getText().replace(",", "."));

			this.troco = ptroco;
			this.valorRecebido = new BigDecimal(this.jtfValorRecebido.getText().replace(".", "").replace(",", "."));
			valorTotal = this.valorTotal.subtract(this.valorRecebido);
			this.jlfValorPagar.setText(this.valorTotal.toString());
			this.formaPagamento = (ModelFormaPagamento) cbFormaPagamento.getSelectedItem();
			this.tipoPagamento = formaPagamento.getDescricao();
			salvarPagamento();
			if (ptroco.intValue() >= 0) {
				doClose(RET_OK);
			}

		} catch (Exception e) {
			ptroco = BigDecimal.ZERO;
			JOptionPane.showMessageDialog(null, "Informe o valor pago!", "Atenção", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	
	// salvar uma venda de mesa
	private boolean salvarPagamento() {
		ModelPagamento modelPagamento = new ModelPagamento();
		modelPagamento.setFk_forma_pagamento(formaPagamento.getCodigo());
		modelPagamento.setFk_venda(codigoVenda);
		modelPagamento.setValor(getValorRecebido());
		ControllerPagamento controllerPagamento = new ControllerPagamento();
		controllerPagamento.salvarPagamentoController(modelPagamento);
		JOptionPane.showMessageDialog(this, "Registro gravado com sucesso!");
		//
		// for (int i = 0; i < tbProdutos.getRowCount(); i++) {
		// modelVendas = new ModelVendas();
		// modelProdutos = new ModelProdutos();
		// modelVendas.setValorTotal(viewPagamento.getValorRecebido());
		// modelVendas.setCodigoUsuario(ModelSessaoUsuario.codigo);
		// try {
		// modelVendas.setDataVenda(bLMascaras.converterDataParaDateUS(new java.util.Date(System.currentTimeMillis())));
		// } catch (Exception ex) {
		// Logger.getLogger(ViewVenda.class.getName()).log(Level.SEVERE, null, ex);
		// }
		// idProduto = Integer.parseInt(tbProdutos.getValueAt(i, 0).toString());
		// modelVendas.setProdutosCodigo(idProduto);
		// modelVendas.setQuantidade(Float.parseFloat(tbProdutos.getValueAt(i, 3).toString()));
		// modelVendas.setTipoPagamento(controllerFormaPagamento.getFormaPagamentoController(viewPagamento.getTipoPagamento()).getCodigo());
		// modelVendas.setValor(Double.parseDouble(tbProdutos.getValueAt(i, 5).toString()));
		// modelProdutos.setId(idProduto);
		//
		// modelProdutos.setValor(new BigDecimal(tbProdutos.getValueAt(i, 5).toString()));
		// listaModelVendas.add(modelVendas);
		// listaProdutoses.add(modelProdutos);
		// }
		// modelVendas.setListamModelVendases(listaModelVendas);
		// modelProdutos.setListaModelProdutoses(listaProdutoses);
		//
		// // salvar venda
		// codigoVenda = controllerVendas.salvarVendasController(modelVendas);
		// if (codigoVenda > 0) {
		// modelVendas.setCodigo(codigoVenda);
		// // salvar lista de produtos
		// controllerVendas.salvarVendasProdutosController(modelVendas);
		// JOptionPane.showMessageDialog(this, "Registro gravado com sucesso!");
		// // adicionarValorCaixa();
		// // imprimir cupom
		// // imprimirCupom();
		//
		// return true;
		// } else {
		// JOptionPane.showMessageDialog(this, "Erro ao gravar os dados!", "ERRO", JOptionPane.ERROR_MESSAGE);
		return false;
		// }

	}

	private void closeDialog(java.awt.event.WindowEvent evt) {
		doClose(RET_CANCEL);
	}

	private void jtfValorRecebidoFocusLost(java.awt.event.FocusEvent evt) {// GEN-FIRST:event_jtfValorRecebidoFocusLost
		this.jtfTroco.setText(this.calcularTroco() + "");
	}

	private void jtfValorRecebidoKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_jtfValorRecebidoKeyReleased
		this.jtfValorRecebido.setText(this.jtfValorRecebido.getText());
	}

	private void doClose(int retStatus) {
		returnStatus = retStatus;
		setVisible(false);
		dispose();
	}

	// verificar permissão do usuário
	public boolean retornarCodigoUsuarioLogado() {
		try {
			String permissao = new ControllerPermissaousuario().getPermissaousuarioCodUsuController(new ModelSessaoUsuario().codigo).getPermissao();
			if (permissao.equals("compras")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	private BigDecimal calcularValorRecebido() {
		return null;
	}

	private BigDecimal calcularDesconto() {
		BigDecimal pSubTotal = BigDecimal.ZERO;
		BigDecimal pDesconto = BigDecimal.ZERO;
		if (this.jtfSubtotal.getText().equals("")) {
			pSubTotal = BigDecimal.ZERO;
		} else {
			pSubTotal = new BigDecimal(this.jtfSubtotal.getText().replace(",", "."));
		}
		valorTotal = pSubTotal.subtract(pDesconto);
		return valorTotal;
	}

	private BigDecimal calcularTroco() {
		BigDecimal pValorRecebido = BigDecimal.ZERO;
		if (!this.jtfValorRecebido.getText().equals("")) {
			pValorRecebido = new BigDecimal(this.jtfValorRecebido.getText().replace(",", "."));
		}
		if (pValorRecebido.doubleValue() > valorTotal.doubleValue()) {
			jtfTroco.setBackground(new Color(127, 255, 212));
		} else {
			jtfTroco.setBackground(new Color(255, 204, 204));
		}
		return pValorRecebido.subtract(valorTotal);
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
		// (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel. For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(ViewPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(ViewPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(ViewPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(ViewPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/* Create and display the dialog */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				ViewPagamento dialog = new ViewPagamento(new JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private JLabel jLabel10;
	private JLabel jLabel4;
	private JLabel jLabel6;
	private JPanel jPanel1;
	private JPanel jPanel2;
	private JLabel jlRS;
	private JLabel jlfValorPagar;
	private JLabel jtfSubtotal;
	private JTextField jtfTroco;
	private JTextField jtfValorRecebido;
	private JButton okButton;
	// End of variables declaration//GEN-END:variables

	private int returnStatus = RET_CANCEL;

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public void setTextFildValorTotal() {
		// setar valor total
		this.jtfSubtotal.setText(this.valorTotal + "");
		jlfValorPagar.setText(jtfSubtotal.getText());
		// limpar interface
		jtfValorRecebido.setText("0");
	}

	public BigDecimal getValorRecebido() {
		return valorRecebido;
	}

	public void setValorRecebido(BigDecimal valorRecebido) {
		this.valorRecebido = valorRecebido;
	}

	public BigDecimal getTroco() {
		return troco;
	}

	public void setTroco(BigDecimal troco) {
		this.troco = troco;
	}

	public BigDecimal getTotalPagar() {
		return totalPagar;
	}

	public void setTotalPagar(BigDecimal totalPagar) {
		this.totalPagar = totalPagar;
	}

	public String getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(String tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public BigDecimal getValorCusto() {
		return valorCusto;
	}

	public void setValorCusto(BigDecimal valorCusto) {
		this.valorCusto = valorCusto;
	}

	public ModelFormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(ModelFormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public int getCodigoVenda() {
		return codigoVenda;
	}

	public void setCodigoVenda(int codigoVenda) {
		this.codigoVenda = codigoVenda;
	}

}
