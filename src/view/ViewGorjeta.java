package view;

import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.MathContext;

import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class ViewGorjeta extends JDialog {

	private BigDecimal valorGorjeta = BigDecimal.ZERO;
	private BigDecimal valorTotal;
	private Float percentualGorjeta = 0F;
	// private ViewVerificarPermissao viewVerificarPermissao;

	public ViewGorjeta(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
		// this.viewVerificarPermissao = new ViewVerificarPermissao(null, true);

		// Close the dialog when Esc is pressed
		String cancelName = "cancel";
		InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
		ActionMap actionMap = getRootPane().getActionMap();
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {
		okButton = new JButton();
		jPanel1 = new JPanel();

		setTitle("Pagamento");
		okButton.setIcon(new ImageIcon(getClass().getResource("/imagens/icons 30/icons8-selecionado-filled-50.png"))); // NOI18N
		okButton.setText("OK");
		okButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				okButtonActionPerformed(evt);
			}
		});

		jPanel1.setBorder(BorderFactory.createTitledBorder(null, "Gorgeta", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 24)));

		tfPercentualGorjeta = new JTextField();
		tfPercentualGorjeta.setColumns(10);

		GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING).addGroup(Alignment.TRAILING,
				jPanel1Layout.createSequentialGroup().addContainerGap(156, Short.MAX_VALUE).addComponent(tfPercentualGorjeta, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE).addGap(149)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addComponent(tfPercentualGorjeta, GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE).addGap(9)));
		jPanel1.setLayout(jPanel1Layout);

		GroupLayout layout = new GroupLayout(getContentPane());
		layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE).addContainerGap())
						.addGroup(Alignment.TRAILING, layout.createSequentialGroup().addComponent(okButton, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE).addGap(160)))));
		layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(50).addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(ComponentPlacement.RELATED).addComponent(okButton, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addContainerGap(324, Short.MAX_VALUE)));
		getContentPane().setLayout(layout);

		getRootPane().setDefaultButton(okButton);

		pack();
	}

	private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {
		try {
			percentualGorjeta = Float.parseFloat(tfPercentualGorjeta.getText());
			BigDecimal gorjeta = new BigDecimal((percentualGorjeta / 100), new MathContext(2));
			this.setValorGorjeta(gorjeta.multiply(getValorTotal(), new MathContext(2)));
			doClose();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "valor inválido!", "Atenção", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private void doClose() {
		setVisible(false);
		dispose();
	}

	public static void main(String args[]) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(ViewGorjeta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(ViewGorjeta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(ViewGorjeta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(ViewGorjeta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/* Create and display the dialog */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				ViewGorjeta dialog = new ViewGorjeta(new JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}
	private JPanel jPanel1;
	private JButton okButton;
	// End of variables declaration//GEN-END:variables

	private JTextField tfPercentualGorjeta;

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public BigDecimal getValorGorjeta() {
		return valorGorjeta;
	}

	public void setValorGorjeta(BigDecimal valorGorgeta) {
		this.valorGorjeta = valorGorgeta;
	}
}
