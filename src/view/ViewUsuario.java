package view;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import controller.ControllerPermissaousuario;
import controller.ControllerUsuario;
import model.ModelConfig;
import model.ModelPermissaousuario;
import model.ModelUsuario;
import util.BLMascaras;
import util.ManipularXML;

public class ViewUsuario extends javax.swing.JFrame {

	ControllerUsuario controllerUsuario = new ControllerUsuario();
	ControllerPermissaousuario controllerPermissaousuario = new ControllerPermissaousuario();
	ModelPermissaousuario modelPermissaousuario = new ModelPermissaousuario();
	ArrayList<ModelPermissaousuario> listaModelPermissaousuarios = new ArrayList<>();
	ModelUsuario modelUsuario = new ModelUsuario();
	String salvarAlterar = "salvar";
	ArrayList<ModelUsuario> listaUsuarios = new ArrayList<>();
	ModelConfig modelConfig = new ModelConfig();
	ManipularXML manipularXML = new ManipularXML();

	public ViewUsuario() {
		initComponents();
		// carregarDadosDoBanco();
		setLocationRelativeTo(null);
		novoUsuario();
	}

	// private void carregarDadosDoBanco() {
	// modelConfig = new ModelConfig();
	// modelConfig = manipularXML.lerXml3();
	// ModelSessaoUsuario.ipServidor = modelConfig.getIp();
	// ModelSessaoUsuario.nomeDoBanco = modelConfig.getNomeBanco();
	// ModelSessaoUsuario.usuarioBanco = modelConfig.getUsuario();
	// ModelSessaoUsuario.senhaBanco = modelConfig.getSenha();
	// ModelSessaoUsuario.caminhoMySQL = modelConfig.getCaminhoMySQL();
	// }
	//

	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		buttonGroup1 = new javax.swing.ButtonGroup();
		jTabbedPaneTipoProduto = new javax.swing.JTabbedPane();
		jpConsulta = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		jTableUsuarios = new javax.swing.JTable();
		jbExcluir = new javax.swing.JButton();
		jButtonAlterar = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Usuários");
		setIconImage(new ImageIcon(getClass().getResource("/imagens/blicon.png")).getImage());
		setResizable(false);
		getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

		jTableUsuarios.setAutoCreateRowSorter(true);
		jTableUsuarios.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null } }, new String[] { "Código", "Nome", "Login" }) {
			Class[] types = new Class[] { java.lang.Integer.class, java.lang.String.class, java.lang.String.class };
			boolean[] canEdit = new boolean[] { false, false, false };

			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		});
		jTableUsuarios.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
		jTableUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		jTableUsuarios.getTableHeader().setReorderingAllowed(false);
		jScrollPane2.setViewportView(jTableUsuarios);
		if (jTableUsuarios.getColumnModel().getColumnCount() > 0) {
			jTableUsuarios.getColumnModel().getColumn(0).setPreferredWidth(90);
			jTableUsuarios.getColumnModel().getColumn(1).setPreferredWidth(250);
			jTableUsuarios.getColumnModel().getColumn(2).setPreferredWidth(250);
		}

		jbExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-excluir-filled-50.png"))); // NOI18N
		jbExcluir.setText("Excluir");
		jbExcluir.setToolTipText("Excluir tipo de produto selecionado");
		jbExcluir.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbExcluirActionPerformed(evt);
			}
		});

		jButtonAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-editar-filled-50.png"))); // NOI18N
		jButtonAlterar.setText("Alterar");
		jButtonAlterar.setToolTipText("Alterar tipo de produto selecionado");
		jButtonAlterar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButtonAlterarActionPerformed(evt);
			}
		});
		jpCadastro = new javax.swing.JPanel();
		jtfCodigo = new javax.swing.JTextField();
		jtfCodigo.setBounds(10, 30, 85, 29);
		jLabel1 = new javax.swing.JLabel();
		jLabel1.setBounds(10, 10, 85, 20);
		jtfNome = new javax.swing.JTextField();
		jtfNome.setBounds(100, 30, 400, 29);
		jLabel2 = new javax.swing.JLabel();
		jLabel2.setBounds(100, 10, 400, 20);
		jbCadastrar = new javax.swing.JButton();
		jbCadastrar.setBounds(357, 330, 150, 29);
		jbLimpar = new javax.swing.JButton();
		jbLimpar.setBounds(10, 330, 150, 29);
		jLabel3 = new javax.swing.JLabel();
		jLabel3.setBounds(10, 70, 210, 20);
		jtfConfirmarSenha = new javax.swing.JPasswordField();
		jtfConfirmarSenha.setBounds(370, 90, 130, 29);
		jLabel4 = new javax.swing.JLabel();
		jLabel4.setBounds(230, 70, 130, 20);
		jtfSenha = new javax.swing.JPasswordField();
		jtfSenha.setBounds(230, 90, 130, 29);
		jLabel5 = new javax.swing.JLabel();
		jLabel5.setBounds(370, 70, 130, 20);
		jtfLogin = new javax.swing.JTextField();
		jtfLogin.setBounds(10, 90, 210, 29);
		jPanel1 = new javax.swing.JPanel();
		jPanel1.setBounds(10, 205, 490, 123);
		jcbUsuario = new javax.swing.JCheckBox();
		jcbUsuario.setBounds(170, 30, 150, 23);
		jcbVendas = new javax.swing.JCheckBox();
		jcbVendas.setBounds(170, 60, 150, 23);
		jcbProdutos = new javax.swing.JCheckBox();
		jcbProdutos.setBounds(18, 30, 150, 23);
		jcbContasPagar = new javax.swing.JCheckBox();
		jcbContasPagar.setBounds(18, 90, 150, 23);
		jcbContasReceber = new javax.swing.JCheckBox();
		jcbContasReceber.setBounds(340, 60, 144, 23);
		jcbCaixa = new javax.swing.JCheckBox();
		jcbCaixa.setBounds(340, 90, 144, 23);
		jcbFormaPagamento = new javax.swing.JCheckBox();
		jcbFormaPagamento.setBounds(340, 30, 144, 23);
		jcbFornecedores = new javax.swing.JCheckBox();
		jcbFornecedores.setBounds(18, 60, 150, 23);
		jcbCompras = new javax.swing.JCheckBox();
		jcbCompras.setBounds(170, 86, 150, 23);
		jpCadastro.setLayout(null);

		jtfCodigo.setEditable(false);
		jtfCodigo.setToolTipText("Código do tipo do produto");
		jtfCodigo.setEnabled(false);
		jpCadastro.add(jtfCodigo);

		jLabel1.setText("Código:");
		jpCadastro.add(jLabel1);

		jtfNome.setToolTipText("Descrição do tipo do produto");
		jtfNome.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent evt) {
				jtfNomeFocusLost(evt);
			}
		});
		jpCadastro.add(jtfNome);

		jLabel2.setText("Nome:");
		jpCadastro.add(jLabel2);

		jbCadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-salvar-filled-50.png"))); // NOI18N
		jbCadastrar.setText("Salvar");
		jbCadastrar.setToolTipText("Salvar cadastro do tipo de produto");
		jbCadastrar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbCadastrarActionPerformed(evt);
			}
		});
		jpCadastro.add(jbCadastrar);

		jbLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-fechar-janela-filled-50.png"))); // NOI18N
		jbLimpar.setText("Limpar");
		jbLimpar.setToolTipText("Cancelar operação");
		jbLimpar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbLimparActionPerformed(evt);
			}
		});
		jpCadastro.add(jbLimpar);

		jLabel3.setText("Login:");
		jpCadastro.add(jLabel3);
		jpCadastro.add(jtfConfirmarSenha);

		jLabel4.setText("Senha:");
		jpCadastro.add(jLabel4);
		jpCadastro.add(jtfSenha);

		jLabel5.setText("Confirmar senha:");
		jpCadastro.add(jLabel5);
		jpCadastro.add(jtfLogin);

		jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Permissões"));
		jPanel1.setLayout(null);

		jcbUsuario.setText("Usuários");
		jPanel1.add(jcbUsuario);

		jcbVendas.setText("Vendas e PDV");
		jcbVendas.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jcbVendasActionPerformed(evt);
			}
		});
		jPanel1.add(jcbVendas);

		jcbProdutos.setText("Produtos");
		jPanel1.add(jcbProdutos);

		jcbContasPagar.setText("Contas a pagar");
		jPanel1.add(jcbContasPagar);

		jcbContasReceber.setText("Contas a receber");
		jPanel1.add(jcbContasReceber);

		jcbCaixa.setText("Caixa");
		jcbCaixa.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jcbCaixaActionPerformed(evt);
			}
		});
		jPanel1.add(jcbCaixa);

		jcbFormaPagamento.setText("Forma de pagamento");
		jPanel1.add(jcbFormaPagamento);

		jcbFornecedores.setText("Fornecedores");
		jPanel1.add(jcbFornecedores);

		jcbCompras.setText("Compras e Estoque");
		jPanel1.add(jcbCompras);

		jpCadastro.add(jPanel1);

		jTabbedPaneTipoProduto.addTab("Cadastro", jpCadastro);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Cancelamento de itens", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 121, 490, 82);
		jpCadastro.add(panel);
		panel.setLayout(null);

		JLabel label = new JLabel();
		label.setBounds(10, 23, 205, 20);
		panel.add(label);
		label.setText("Senha:");

		jtfSenhaCancelamento = new JPasswordField();
		jtfSenhaCancelamento.setBounds(10, 43, 205, 31);
		panel.add(jtfSenhaCancelamento);
		jtfSenhaCancelamento.setText("");

		JLabel label_1 = new JLabel();
		label_1.setBounds(225, 23, 218, 20);
		panel.add(label_1);
		label_1.setText("Confirmar senha:");

		jtfConfirmarSenhaCancelamento = new JPasswordField();
		jtfConfirmarSenhaCancelamento.setBounds(225, 43, 218, 31);
		panel.add(jtfConfirmarSenhaCancelamento);
		jtfConfirmarSenhaCancelamento.setText("");

		javax.swing.GroupLayout jpConsultaLayout = new javax.swing.GroupLayout(jpConsulta);
		jpConsulta.setLayout(jpConsultaLayout);
		jpConsultaLayout
				.setHorizontalGroup(jpConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jpConsultaLayout.createSequentialGroup().addContainerGap()
								.addGroup(jpConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jScrollPane2)
										.addGroup(jpConsultaLayout.createSequentialGroup().addComponent(jbExcluir).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 329, Short.MAX_VALUE).addComponent(jButtonAlterar)))
								.addContainerGap()));
		jpConsultaLayout.setVerticalGroup(jpConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jpConsultaLayout.createSequentialGroup().addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 317, Short.MAX_VALUE)
						.addGroup(jpConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jpConsultaLayout.createSequentialGroup().addGap(15, 15, 15).addComponent(jbExcluir))
								.addGroup(jpConsultaLayout.createSequentialGroup().addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jButtonAlterar)))
						.addContainerGap()));

		jTabbedPaneTipoProduto.addTab("Consulta/Alteração/Exclusão", jpConsulta);

		getContentPane().add(jTabbedPaneTipoProduto, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 530, 400));

		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void jbCadastrarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jbCadastrarActionPerformed
		// TODO add your handling code here:
		if (this.jtfNome.getText().equals("") || this.jtfLogin.getText().equals("")) {
			JOptionPane.showMessageDialog(rootPane, "Você deve informar o nome para salvar!", "Atenção!", JOptionPane.WARNING_MESSAGE);
			return;
		}

		String senha;
		String confirmaSenha;
		String senhaCancelamento;
		String confirmaSenhaCancelamento;
		senha = new String(this.jtfSenha.getPassword());
		confirmaSenha = new String(this.jtfConfirmarSenha.getPassword());
		senhaCancelamento = new String(this.jtfSenhaCancelamento.getPassword());
		confirmaSenhaCancelamento = new String(this.jtfConfirmarSenhaCancelamento.getPassword());

		// testa se as senhas são iguais
		if (senha.equals(confirmaSenha)) {
			if (senhaCancelamento.equals(confirmaSenhaCancelamento)) {
				if (salvarAlterar.equals("salvar")) {
					this.salvarUsuario();
				} else if (salvarAlterar.equals("alterar")) {
					this.alterarUsuario();
				}
			} else {
				JOptionPane.showMessageDialog(rootPane, "As senhas e cancelamento de itens digitadas não conferem!", "Aviso", JOptionPane.WARNING_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(rootPane, "As senhas digitadas não conferem!", "Aviso", JOptionPane.WARNING_MESSAGE);
		}
		// salvar tipo produto
	}// GEN-LAST:event_jbCadastrarActionPerformed

	private void jbExcluirActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jbExcluirActionPerformed
		// TODO add your handling code here:
		int linha = jTableUsuarios.getSelectedRow();
		String nome = (String) jTableUsuarios.getValueAt(linha, 1);
		int codigo = (int) jTableUsuarios.getValueAt(linha, 0);

		// pegunta se realmente deseja excluir o tipo de produto
		int opcao = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja" + " excluir o usuário:\n" + "\n " + nome + "?", "Atenção", JOptionPane.YES_NO_OPTION);
		// se sim exclui, se não não faz nada
		if (opcao == JOptionPane.OK_OPTION) {
			if (controllerPermissaousuario.excluirPermissaousuarioController(codigo)) {
				controllerUsuario.excluirUsuarioController(codigo);
				JOptionPane.showMessageDialog(this, "Registro excluido com suscesso!");
				this.novoUsuario();
			}
		}
	}// GEN-LAST:event_jbExcluirActionPerformed

	private void jbLimparActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jbLimparActionPerformed
		// TODO add your handling code here:
		novoUsuario();
	}// GEN-LAST:event_jbLimparActionPerformed

	private void jButtonAlterarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonAlterarActionPerformed
		desativarPermissao();
		this.recuperarUsuario();

		// volta a aba anterior
		this.jTabbedPaneTipoProduto.setSelectedIndex(this.jTabbedPaneTipoProduto.getSelectedIndex() - 1);
		salvarAlterar = "alterar";
	}// GEN-LAST:event_jButtonAlterarActionPerformed

	private void jcbVendasActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jcbVendasActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_jcbVendasActionPerformed

	private void jcbCaixaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jcbCaixaActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_jcbCaixaActionPerformed

	private void jtfNomeFocusLost(java.awt.event.FocusEvent evt) {// GEN-FIRST:event_jtfNomeFocusLost
		// converte em maiusculo o que o usuario digitar
		this.jtfNome.setText(jtfNome.getText().toUpperCase());
		this.jtfNome.setText(new BLMascaras().TiraAcentos(jtfNome.getText()));
	}// GEN-LAST:event_jtfNomeFocusLost

	/**
	 * Salvar
	 *
	 * @return
	 */
	private boolean salvarUsuario() {
		ModelUsuario modelUsuario = new ModelUsuario();
		int codigousuario;
		modelUsuario.setNome(this.jtfNome.getText());
		modelUsuario.setLogin(this.jtfLogin.getText());
		modelUsuario.setSenha(new String(this.jtfSenha.getPassword()));
		modelUsuario.setSenhaCancelamento(new String(this.jtfSenhaCancelamento.getPassword()));

		codigousuario = controllerUsuario.salvarUsuarioController(modelUsuario);
		// salvar
		if (codigousuario > 0) {
			controllerPermissaousuario.salvarPermissaousuarioController(setardadosPermissaoUsuario(codigousuario));
			JOptionPane.showMessageDialog(this, "Registro gravado com sucesso!");
			this.novoUsuario();
			jTabbedPaneTipoProduto.setSelectedIndex(jTabbedPaneTipoProduto.getSelectedIndex() + 1);
			return true;
		} else {
			JOptionPane.showMessageDialog(this, "Erro ao gravar os dados!", "ERRO", JOptionPane.ERROR_MESSAGE);
			return false;
		}

	}

	public ModelPermissaousuario setardadosPermissaoUsuario(int pCodigoUsuario) {
		modelPermissaousuario = new ModelPermissaousuario();
		if (jcbProdutos.isSelected()) {
			modelPermissaousuario = new ModelPermissaousuario();
			modelPermissaousuario.setCodigo_usuario(pCodigoUsuario);
			modelPermissaousuario.setPermissao("produto");
			listaModelPermissaousuarios.add(modelPermissaousuario);
		}
		if (jcbUsuario.isSelected()) {
			modelPermissaousuario = new ModelPermissaousuario();
			modelPermissaousuario.setCodigo_usuario(pCodigoUsuario);
			modelPermissaousuario.setPermissao("usuario");
			listaModelPermissaousuarios.add(modelPermissaousuario);
		}
		if (jcbVendas.isSelected()) {
			modelPermissaousuario = new ModelPermissaousuario();
			modelPermissaousuario.setCodigo_usuario(pCodigoUsuario);
			modelPermissaousuario.setPermissao("venda");
			listaModelPermissaousuarios.add(modelPermissaousuario);
		}
		if (jcbFormaPagamento.isSelected()) {
			modelPermissaousuario = new ModelPermissaousuario();
			modelPermissaousuario.setCodigo_usuario(pCodigoUsuario);
			modelPermissaousuario.setPermissao("pagamento");
			listaModelPermissaousuarios.add(modelPermissaousuario);
		}
		if (jcbCompras.isSelected()) {
			modelPermissaousuario = new ModelPermissaousuario();
			modelPermissaousuario.setCodigo_usuario(pCodigoUsuario);
			modelPermissaousuario.setPermissao("compras");
			listaModelPermissaousuarios.add(modelPermissaousuario);
		}
		if (jcbContasPagar.isSelected()) {
			modelPermissaousuario = new ModelPermissaousuario();
			modelPermissaousuario.setCodigo_usuario(pCodigoUsuario);
			modelPermissaousuario.setPermissao("pagar");
			listaModelPermissaousuarios.add(modelPermissaousuario);
		}
		if (jcbContasReceber.isSelected()) {
			modelPermissaousuario = new ModelPermissaousuario();
			modelPermissaousuario.setCodigo_usuario(pCodigoUsuario);
			modelPermissaousuario.setPermissao("receber");
			listaModelPermissaousuarios.add(modelPermissaousuario);
		}
		if (jcbCaixa.isSelected()) {
			modelPermissaousuario = new ModelPermissaousuario();
			modelPermissaousuario.setCodigo_usuario(pCodigoUsuario);
			modelPermissaousuario.setPermissao("caixa");
			listaModelPermissaousuarios.add(modelPermissaousuario);
		}
		if (jcbFornecedores.isSelected()) {
			modelPermissaousuario = new ModelPermissaousuario();
			modelPermissaousuario.setCodigo_usuario(pCodigoUsuario);
			modelPermissaousuario.setPermissao("fornecedor");
			listaModelPermissaousuarios.add(modelPermissaousuario);
		}
		modelPermissaousuario.setListaModelPermissaousuarios(listaModelPermissaousuarios);
		return modelPermissaousuario;
	}

	private boolean alterarUsuario() {
		modelUsuario.setCodigo(Integer.parseInt(this.jtfCodigo.getText()));
		modelUsuario.setNome(this.jtfNome.getText());
		modelUsuario.setLogin(this.jtfLogin.getText());
		modelUsuario.setSenha(new String(this.jtfSenha.getPassword()));
		modelUsuario.setSenhaCancelamento(new String(this.jtfSenhaCancelamento.getPassword()));

		// alterar
		if (controllerUsuario.atualizarUsuarioController(modelUsuario)) {
			// exclui as permissões antigas
			controllerPermissaousuario.excluirPermissaousuarioController(modelUsuario.getCodigo());
			// cadastra as novas
			controllerPermissaousuario.salvarPermissaousuarioController(setardadosPermissaoUsuario(modelUsuario.getCodigo()));
			JOptionPane.showMessageDialog(this, "Registro alterado com sucesso!");
			this.carregarUsuarios();
			jTabbedPaneTipoProduto.setSelectedIndex(jTabbedPaneTipoProduto.getSelectedIndex() + 1);
			return true;
		} else {
			JOptionPane.showMessageDialog(this, "Erro ao alterar os dados!", "ERRO", JOptionPane.ERROR_MESSAGE);
			return false;
		}

	}

	private void carregarUsuarios() {
		listaUsuarios = controllerUsuario.getListaUsuarioController();

		DefaultTableModel modelo = (DefaultTableModel) jTableUsuarios.getModel();
		modelo.setNumRows(0);

		// CARREGA OS DADOS DA LISTA NA TABELA
		int cont = listaUsuarios.size();
		for (int i = 0; i < cont; i++) {
			modelo.addRow(new Object[] { listaUsuarios.get(i).getCodigo(), listaUsuarios.get(i).getNome(), listaUsuarios.get(i).getLogin() });
		}
	}

	/**
	 * Pega os dados
	 *
	 * @return boolean
	 */
	private boolean recuperarUsuario() {
		// recebe a linha selecionada
		int linha = this.jTableUsuarios.getSelectedRow();

		// pega o codigo do cliente na linha selecionada
		int codigo = (Integer) jTableUsuarios.getValueAt(linha, 0);

		try {
			// recupera os dados do banco
			modelUsuario = controllerUsuario.getUsuarioController(codigo);
			listaModelPermissaousuarios = controllerPermissaousuario.getListaPermissaousuarioController(codigo);
			// seta os dados na interface
			this.jtfCodigo.setText(String.valueOf(modelUsuario.getCodigo()));
			this.jtfNome.setText(modelUsuario.getNome());
			this.jtfLogin.setText(modelUsuario.getLogin());
			this.jtfSenha.setText(modelUsuario.getSenha());
			this.jtfConfirmarSenha.setText(modelUsuario.getSenha());
			// preencher permissoes
			for (int i = 0; i < listaModelPermissaousuarios.size(); i++) {
				if (listaModelPermissaousuarios.get(i).getPermissao().equals("produto")) {
					jcbProdutos.setSelected(true);
				}
				if (listaModelPermissaousuarios.get(i).getPermissao().equals("fornecedor")) {
					jcbFornecedores.setSelected(true);
				}
				if (listaModelPermissaousuarios.get(i).getPermissao().equals("usuario")) {
					jcbUsuario.setSelected(true);
				}
				if (listaModelPermissaousuarios.get(i).getPermissao().equals("venda")) {
					jcbVendas.setSelected(true);
				}
				if (listaModelPermissaousuarios.get(i).getPermissao().equals("pagar")) {
					jcbContasPagar.setSelected(true);
				}
				if (listaModelPermissaousuarios.get(i).getPermissao().equals("receber")) {
					jcbContasReceber.setSelected(true);
				}
				if (listaModelPermissaousuarios.get(i).getPermissao().equals("pagamento")) {
					jcbFormaPagamento.setSelected(true);
				}
				if (listaModelPermissaousuarios.get(i).getPermissao().equals("caixa")) {
					jcbCaixa.setSelected(true);
				}
				if (listaModelPermissaousuarios.get(i).getPermissao().equals("compras")) {
					jcbCompras.setSelected(true);
				}
			}

			return true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Código inválido ou nenhum registro selecionado", "Aviso", JOptionPane.WARNING_MESSAGE);
			return false;
		}

	}

	private void novoUsuario() {
		jtfCodigo.setText("Novo");
		jtfNome.setText("");
		jtfLogin.setText("");
		jtfSenha.setText("");
		jtfConfirmarSenha.setText("");
		jtfSenhaCancelamento.setText("");
		jtfConfirmarSenhaCancelamento.setText("");
		desativarPermissao();
		salvarAlterar = "salvar";
		carregarUsuarios();

	}

	private void desativarPermissao() {
		jcbProdutos.setSelected(false);
		jcbUsuario.setSelected(false);
		jcbVendas.setSelected(false);
		jcbFormaPagamento.setSelected(false);
		jcbCaixa.setSelected(false);
		jcbFornecedores.setSelected(false);
		jcbContasPagar.setSelected(false);
		jcbContasReceber.setSelected(false);

	}

	public static void main(String args[]) {

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {
				new ViewUsuario().setVisible(true);
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.ButtonGroup buttonGroup1;
	private javax.swing.JButton jButtonAlterar;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JTabbedPane jTabbedPaneTipoProduto;
	private javax.swing.JTable jTableUsuarios;
	private javax.swing.JButton jbCadastrar;
	private javax.swing.JButton jbExcluir;
	private javax.swing.JButton jbLimpar;
	private javax.swing.JCheckBox jcbCaixa;
	private javax.swing.JCheckBox jcbCompras;
	private javax.swing.JCheckBox jcbContasPagar;
	private javax.swing.JCheckBox jcbContasReceber;
	private javax.swing.JCheckBox jcbFormaPagamento;
	private javax.swing.JCheckBox jcbFornecedores;
	private javax.swing.JCheckBox jcbProdutos;
	private javax.swing.JCheckBox jcbUsuario;
	private javax.swing.JCheckBox jcbVendas;
	private javax.swing.JPanel jpCadastro;
	private javax.swing.JPanel jpConsulta;
	private javax.swing.JTextField jtfCodigo;
	private javax.swing.JPasswordField jtfConfirmarSenha;
	private javax.swing.JTextField jtfLogin;
	private javax.swing.JTextField jtfNome;
	private javax.swing.JPasswordField jtfSenha;
	private JPasswordField jtfSenhaCancelamento;
	private JPasswordField jtfConfirmarSenhaCancelamento;
}
