package view;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import commons.report.pkg1.pkg8.pkg2.vreport;
import controller.ControllerFormaPagamento;
import controller.ControllerPermissaousuario;
import controller.ControllerProdutos;
import controller.ControllerUnidadeMedia;
import model.ModelFormaPagamento;
import model.ModelPermissaousuario;
import model.ModelProdutos;
import model.ModelSessaoUsuario;
import model.ModelUnidadeMedia;
import util.AguardeGerandoRelatorio;
import util.BLMascaras;
import util.Carregando;

public class ViewPrincipal extends javax.swing.JFrame {

    String pNomeUsuario;
    ControllerProdutos controllerProdutos = new ControllerProdutos();
    ControllerUnidadeMedia controllerUnidadeMedia = new ControllerUnidadeMedia();
    ControllerFormaPagamento controllerFormaPagamento = new ControllerFormaPagamento();   
    ArrayList<ModelProdutos> modelProdutos = new ArrayList<>();
    ArrayList<ModelUnidadeMedia> modelUnidadeMedia = new ArrayList<>();
    ArrayList<ModelFormaPagamento> modelFormaPagamentos = new ArrayList<>();
    BLMascaras bLMascaras = new BLMascaras();

    public ViewPrincipal() {

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        initComponents();
        configurar();
        liberarModulos();
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        uJPanelImagem1 = new componentes.UJPanelImagem();
        jPanel1 = new javax.swing.JPanel();
        jToolBar2 = new javax.swing.JToolBar();
        btProdutos = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        btMesa = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        jLabel8 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jlUsuario = new javax.swing.JLabel();
        jlData = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jmArquivo = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JSeparator();
        mnuSair = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jmiBackup = new javax.swing.JMenuItem();
        jmCadastrar = new javax.swing.JMenu();
        jmiProdutos = new javax.swing.JMenuItem();
        jmiUsuario = new javax.swing.JMenuItem();
        miFormaPagamento = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jmiRelProdutos = new javax.swing.JMenuItem();
        jmiRelatorioVendasPeriodo = new javax.swing.JMenuItem();
        jmiRelatorioProdutosVendidos = new javax.swing.JMenuItem();
        jmAjuda = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        jMenu2.setText("File");
        jMenuBar2.add(jMenu2);

        jMenu3.setText("Edit");
        jMenuBar2.add(jMenu3);

        jMenu4.setText("jMenu4");

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BLVendas Pro 4.x");
        setFocusTraversalPolicyProvider(true);
        setIconImage(new ImageIcon(getClass().getResource("/imagens/blicon.png")).getImage());
        setLocationByPlatform(true);

		uJPanelImagem1.setImagem(new java.io.File("C:\\BLVendas\\src\\imagens\\fundo-padre.jpg"));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jToolBar2.setBackground(new java.awt.Color(255, 255, 255));
        jToolBar2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Menu de acesso rápido", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.BELOW_TOP, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N
        jToolBar2.setForeground(new java.awt.Color(102, 102, 102));
        jToolBar2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jToolBar2.setRollover(true);

        btProdutos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 30/icons8-comida-como-recursos-filled-50.png"))); // NOI18N
        btProdutos.setText("Produtos");
        btProdutos.setEnabled(false);
        btProdutos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btProdutos.setMaximumSize(new java.awt.Dimension(200, 40));
        btProdutos.setMinimumSize(new java.awt.Dimension(200, 40));
        btProdutos.setPreferredSize(new java.awt.Dimension(200, 40));
        btProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btProdutosActionPerformed(evt);
            }
        });
        jToolBar2.add(btProdutos);
        jToolBar2.add(jSeparator3);

        btMesa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 30/icons8-carrinho-de-compras-filled-50.png"))); // NOI18N
        btMesa.setText("Mesas");
        btMesa.setEnabled(false);
        btMesa.setFocusable(false);
        btMesa.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btMesa.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btMesa.setMaximumSize(new java.awt.Dimension(200, 40));
        btMesa.setMinimumSize(new java.awt.Dimension(200, 40));
        btMesa.setPreferredSize(new java.awt.Dimension(200, 40));
        btMesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMesaActionPerformed(evt);
            }
        });
        jToolBar2.add(btMesa);
        jToolBar2.add(jSeparator4);
        jToolBar2.add(jSeparator5);

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setIcon(new ImageIcon(ViewPrincipal.class.getResource("/imagens/logo_menor.png"))); // NOI18N
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel8MouseClicked(evt);
            }
        });
        jToolBar2.add(jLabel8);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1Layout.setHorizontalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addComponent(jToolBar2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(jToolBar2, GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE))
        );
		jPanel1.setLayout(jPanel1Layout);

        javax.swing.GroupLayout uJPanelImagem1Layout = new javax.swing.GroupLayout(uJPanelImagem1);
        uJPanelImagem1Layout.setHorizontalGroup(
        	uJPanelImagem1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(uJPanelImagem1Layout.createSequentialGroup()
        			.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(804, Short.MAX_VALUE))
        );
        uJPanelImagem1Layout.setVerticalGroup(
        	uJPanelImagem1Layout.createParallelGroup(Alignment.LEADING)
        		.addComponent(jPanel1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 679, Short.MAX_VALUE)
        );
        uJPanelImagem1.setLayout(uJPanelImagem1Layout);

        jPanel3.setBackground(new java.awt.Color(102, 102, 102));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Logado como:");

        jlUsuario.setForeground(new java.awt.Color(255, 255, 255));
        jlUsuario.setText("usuaario");

        jlData.setForeground(new java.awt.Color(255, 255, 255));
        jlData.setText("data");

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Data/Hora de login:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlData))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jlUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jlData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jmArquivo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-lista-50.png"))); // NOI18N
        jmArquivo.setText("Arquivo");
        jmArquivo.add(jSeparator1);

        mnuSair.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        mnuSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-sair-filled-50.png"))); // NOI18N
        mnuSair.setText("Sair");
        mnuSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuSairActionPerformed(evt);
            }
        });
        jmArquivo.add(mnuSair);
        jmArquivo.add(jSeparator2);

        jmiBackup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-fundo-de-encaixe-filled-50.png"))); // NOI18N
        jmiBackup.setText("Backup");
        jmiBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiBackupActionPerformed(evt);
            }
        });
        jmArquivo.add(jmiBackup);

        jMenuBar1.add(jmArquivo);

        jmCadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-adicionar-usuário-masculino-filled-50.png"))); // NOI18N
        jmCadastrar.setText("Cadastros");

        jmiProdutos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_MASK));
        jmiProdutos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-comida-como-recursos-filled-50.png"))); // NOI18N
        jmiProdutos.setText("Produtos");
        jmiProdutos.setEnabled(false);
        jmiProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProdutosActionPerformed(evt);
            }
        });
        jmCadastrar.add(jmiProdutos);

        jmiUsuario.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.ALT_MASK));
        jmiUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-direitos-de-usuário-filled-50.png"))); // NOI18N
        jmiUsuario.setText("Usuário");
        jmiUsuario.setEnabled(false);
        jmiUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiUsuarioActionPerformed(evt);
            }
        });
        jmCadastrar.add(jmiUsuario);

        miFormaPagamento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-adicionar-filled-50.png"))); // NOI18N
        miFormaPagamento.setText("Forma de pagamento");
        miFormaPagamento.setEnabled(false);
        miFormaPagamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFormaPagamentoActionPerformed(evt);
            }
        });
        jmCadastrar.add(miFormaPagamento);

        jMenuBar1.add(jmCadastrar);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-impressão-filled-50.png"))); // NOI18N
        jMenu1.setText("Relatórios");

        jmiRelProdutos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-pdf-2-filled-50.png"))); // NOI18N
        jmiRelProdutos.setText("Produtos");
        jmiRelProdutos.setEnabled(false);
        jmiRelProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiRelProdutosActionPerformed(evt);
            }
        });
        jMenu1.add(jmiRelProdutos);

        jmiRelatorioVendasPeriodo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-pdf-2-filled-50.png"))); // NOI18N
        jmiRelatorioVendasPeriodo.setText("Vendas por período");
        jmiRelatorioVendasPeriodo.setEnabled(false);
        jmiRelatorioVendasPeriodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiRelatorioVendasPeriodoActionPerformed(evt);
            }
        });
        jMenu1.add(jmiRelatorioVendasPeriodo);

        jmiRelatorioProdutosVendidos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-pdf-2-filled-50.png"))); // NOI18N
        jmiRelatorioProdutosVendidos.setText("Produtos vendidos");
        jmiRelatorioProdutosVendidos.setEnabled(false);
        jmiRelatorioProdutosVendidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiRelatorioProdutosVendidosActionPerformed(evt);
            }
        });
        jMenu1.add(jmiRelatorioProdutosVendidos);

        jMenuBar1.add(jMenu1);

        jmAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-ajuda-filled-50.png"))); // NOI18N
        jmAjuda.setText("Ajuda");

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-e-mail-filled-50.png"))); // NOI18N
        jMenuItem3.setText("Contato");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jmAjuda.add(jMenuItem3);

        jMenuBar1.add(jmAjuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(uJPanelImagem1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(uJPanelImagem1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }

    private void mnuSairActionPerformed(java.awt.event.ActionEvent evt) {
        System.exit(0);
    }
    private void jmiProdutosActionPerformed(java.awt.event.ActionEvent evt) {
        modelUnidadeMedia = controllerUnidadeMedia.getListaUnidadeMediaController();
        if (modelUnidadeMedia.size() < 1 ) {
            JOptionPane.showMessageDialog(this, "Cadastre primeiro unidade de medida!", "ATENÇÂO", JOptionPane.WARNING_MESSAGE);
        } else {
            new ViewProduto().setVisible(true);
        }
    }

    private void jmiRelProdutosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiRelProdutosActionPerformed
        final AguardeGerandoRelatorio carregando = new AguardeGerandoRelatorio();
        final ControllerProdutos controllerProdutos = new ControllerProdutos();
        carregando.setVisible(true);
        Thread t = new Thread() {
            @Override
            public void run() {
                // imprimir produtos
                controllerProdutos.gerarRelatorioProdutos();
                carregando.dispose();
            }
        };
        t.start();
    }//GEN-LAST:event_jmiRelProdutosActionPerformed

    private void jmiUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiUsuarioActionPerformed
        
        new ViewUsuario().setVisible(true);
    }//GEN-LAST:event_jmiUsuarioActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        
        new ViewSobre(this, rootPaneCheckingEnabled).setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jmiRelatorioVendasPeriodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiRelatorioVendasPeriodoActionPerformed
        
        new ViewRelatorioVendas().setVisible(true);
    }//GEN-LAST:event_jmiRelatorioVendasPeriodoActionPerformed

    private void miFormaPagamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFormaPagamentoActionPerformed
        
        new ViewFormaPagamento().setVisible(true);
    }

    private void jmiRelatorioProdutosVendidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiRelatorioProdutosVendidosActionPerformed
        
        new ViewRelatorioProduto().setVisible(true);
    }

    private void jmiBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiBackupActionPerformed
        try {
            
            new ViewBackup().setVisible(true);
        } catch (Exception ex) {
            Logger.getLogger(ViewPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void btProdutosActionPerformed(java.awt.event.ActionEvent evt) {
        final Carregando carregando = new Carregando();
        carregando.setVisible(true);
        Thread t = new Thread() {
            @Override
            public void run() {
                // abrir
                modelUnidadeMedia = controllerUnidadeMedia.getListaUnidadeMediaController();
                if (modelUnidadeMedia.size() < 1) {
                    JOptionPane.showMessageDialog(null, "Cadastre primeiro unidade de medida!", "ATENÇÂO", JOptionPane.WARNING_MESSAGE);
                } else {
                    new ViewProduto().setVisible(true);
                }
                carregando.dispose();
            }
        };
        t.start();
    }

    private void btMesaActionPerformed(java.awt.event.ActionEvent evt) {
        // acessar
        final Carregando carregando = new Carregando();
        carregando.setVisible(true);
        Thread t = new Thread() {
            @Override
            public void run() {
                // abrir
                modelProdutos = controllerProdutos.getListaProdutosAtivosController();
                if (modelProdutos.size() < 1) {
                    JOptionPane.showMessageDialog(null, "Cadastre primeiro produtos!", "ATENÇÂO", JOptionPane.WARNING_MESSAGE);
                } else {
                    new ViewMesas().setVisible(true);
                }
                carregando.dispose();
            }
        };
        t.start();
    }

    private void jLabel8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseClicked
        
        Desktop desk = java.awt.Desktop.getDesktop();
        try {
            desk.browse(new java.net.URI("http://www.blsoft.com.br/"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jLabel8MouseClicked

    public String retornarUsuarioLogado() {
        return new ModelSessaoUsuario().nome;
    }

    private void cria() {
        JButton bt = new JButton("Código fonte");
        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new vreport().setVisible(true);
            }
        });
//        jMenuBar1.add(bt);
    }

    private void configurar() {
        pNomeUsuario = retornarUsuarioLogado();
        jlUsuario.setText(pNomeUsuario);
        jlData.setText(bLMascaras.retornarDataHora());
    }

    private void liberarModulos() {
        ControllerPermissaousuario controllerPermissaousuario = new ControllerPermissaousuario();
        ArrayList<ModelPermissaousuario> listaModelPermissaousuarios = new ArrayList<>();
        listaModelPermissaousuarios = controllerPermissaousuario.getListaPermissaousuarioController(new ModelSessaoUsuario().codigo);
        cria();
        for (int i = 0; i < listaModelPermissaousuarios.size(); i++) {
            if (listaModelPermissaousuarios.get(i).getPermissao().equals("cliente")) {

            }
            if (listaModelPermissaousuarios.get(i).getPermissao().equals("produto")) {
                btProdutos.setEnabled(true);
                jmiRelProdutos.setEnabled(true);
                jmiProdutos.setEnabled(true);
            }
            if (listaModelPermissaousuarios.get(i).getPermissao().equals("pagamento")) {
            	miFormaPagamento.setEnabled(true);
            }
            if (listaModelPermissaousuarios.get(i).getPermissao().equals("usuario")) {
                jmiUsuario.setEnabled(true);
            }
            if (listaModelPermissaousuarios.get(i).getPermissao().equals("venda")) {
                btMesa.setEnabled(true);
                btMesa.setEnabled(true);
                jmiRelatorioVendasPeriodo.setEnabled(true);
                jmiRelatorioProdutosVendidos.setEnabled(true);
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewPrincipal().setVisible(true);
            }
        });
    }
    private javax.swing.JButton btMesa;
    private javax.swing.JButton btProdutos;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JLabel jlData;
    private javax.swing.JLabel jlUsuario;
    private javax.swing.JMenu jmAjuda;
    private javax.swing.JMenu jmArquivo;
    private javax.swing.JMenu jmCadastrar;
    private javax.swing.JMenuItem jmiBackup;
    private javax.swing.JMenuItem jmiProdutos;
    private javax.swing.JMenuItem jmiRelProdutos;
    private javax.swing.JMenuItem jmiRelatorioProdutosVendidos;
    private javax.swing.JMenuItem jmiRelatorioVendasPeriodo;
    private javax.swing.JMenuItem jmiUsuario;
    private javax.swing.JMenuItem miFormaPagamento;
    private javax.swing.JMenuItem mnuSair;
    private componentes.UJPanelImagem uJPanelImagem1;
    // End of variables declaration//GEN-END:variables

    private void JButton() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
