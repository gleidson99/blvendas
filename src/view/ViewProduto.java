/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.SystemColor;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.ControllerProdutos;
import model.ModelProdutos;
import util.BLMascaras;
import util.JMoneyField;

public class ViewProduto extends javax.swing.JFrame {

	ModelProdutos modelProdutos = new ModelProdutos();
	ControllerProdutos controllerProdutos = new ControllerProdutos();
	ArrayList<ModelProdutos> listamModelProdutos = new ArrayList<ModelProdutos>();
	BLMascaras bLMascaras = new BLMascaras();
	String tipoCadastro;

	public ViewProduto() {
		initComponents();
		this.carregarProdutos();
		setLocationRelativeTo(null);
		// novo inicio
		novoProduto();
		// novo fim
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Produtos");
		setIconImage(new ImageIcon(getClass().getResource("/imagens/blicon.png")).getImage());
		setResizable(false);
		jLabel5 = new javax.swing.JLabel();

		jLabel5.setText("Código:");
		jLabel1 = new javax.swing.JLabel();

		jLabel1.setText("Descricao:");
		jtfDescricao = new javax.swing.JTextField();

		jtfDescricao.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent evt) {
				jtfDescricaoFocusLost(evt);
			}
		});
		jtfValor = new JMoneyField(9);
		jLabel2 = new javax.swing.JLabel();

		jLabel2.setText("Situação:");
		jcbAtivo = new javax.swing.JComboBox<>();

		jcbAtivo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Inativo", "Ativo" }));
		ckAgrupa = new JCheckBox();
		ckAgrupa.setText("Pode agrupar");
		tfPesquisaProduto = new javax.swing.JTextField();

		tfPesquisaProduto.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent evt) {
				tfPesquisaProdutoFocusLost(evt);
			}
		});
		jLabel8 = new javax.swing.JLabel();

		jLabel8.setText("Pesquisar:");
		jScrollPane2 = new javax.swing.JScrollPane();
		tbProdutos = new javax.swing.JTable();
		tbProdutos.setSelectionBackground(SystemColor.inactiveCaption);

		tbProdutos.setAutoCreateRowSorter(true);
		tbProdutos.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null, null }, }, new String[] { "Id", "C\u00F3digo", "Descricao", "Valor", "Ativo" }));
		tbProdutos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
		tbProdutos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		tbProdutos.getTableHeader().setReorderingAllowed(false);
		tbProdutos.getColumnModel().getColumn(0).setMinWidth(0);
		tbProdutos.getColumnModel().getColumn(0).setPreferredWidth(0);
		tbProdutos.getColumnModel().getColumn(0).setMaxWidth(0);
		jScrollPane2.setViewportView(tbProdutos);
		btPesquisaProduto = new javax.swing.JButton();

		btPesquisaProduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-binóculos-filled-50.png"))); // NOI18N
		btPesquisaProduto.setText("Pesquisar");
		btPesquisaProduto.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btPesquisaProdutoActionPerformed(evt);
			}
		});
		btCancelar = new javax.swing.JButton();

		btCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-fechar-janela-filled-50.png"))); // NOI18N
		btCancelar.setText("Novo");
		btCancelar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btCancelarActionPerformed(evt);
			}
		});
		btExcluir = new javax.swing.JButton();

		btExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-excluir-filled-50.png"))); // NOI18N
		btExcluir.setText("Excluir");
		btExcluir.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btExcluirActionPerformed(evt);
			}
		});
		btAlterar = new javax.swing.JButton();

		btAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-editar-filled-50.png"))); // NOI18N
		btAlterar.setText("Alterar");
		btAlterar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btAlterarActionPerformed(evt);
			}
		});
		jbSalvar = new javax.swing.JButton();
		jbSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-salvar-filled-50.png"))); // NOI18N
		jbSalvar.setText("Salvar");
		jbSalvar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbSalvarActionPerformed(evt);
			}
		});

		JLabel lblValor = new JLabel();
		lblValor.setText("Valor:");

		jtfCodigo = new JTextField();
		jtfCodigo.setColumns(10);

		ckDesconto = new JCheckBox();
		ckDesconto.setText("Pemite desconto");
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
							.addComponent(btCancelar, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btExcluir, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btAlterar, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 262, Short.MAX_VALUE)
							.addComponent(jbSalvar, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
						.addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 698, Short.MAX_VALUE)
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup(Alignment.LEADING)
								.addGroup(layout.createSequentialGroup()
									.addGroup(layout.createParallelGroup(Alignment.LEADING)
										.addComponent(jLabel1)
										.addComponent(jLabel5)
										.addComponent(jLabel2))
									.addGap(18)
									.addGroup(layout.createParallelGroup(Alignment.LEADING)
										.addGroup(layout.createSequentialGroup()
											.addGroup(layout.createParallelGroup(Alignment.LEADING)
												.addComponent(jtfCodigo, GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
												.addComponent(jcbAtivo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
											.addGap(18)
											.addGroup(layout.createParallelGroup(Alignment.LEADING)
												.addGroup(layout.createSequentialGroup()
													.addComponent(ckAgrupa)
													.addGap(18)
													.addComponent(ckDesconto, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
												.addGroup(layout.createSequentialGroup()
													.addComponent(lblValor, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(jtfValor, GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE))))
										.addComponent(jtfDescricao, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED, 0, Short.MAX_VALUE))
								.addGroup(layout.createSequentialGroup()
									.addComponent(jLabel8)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(tfPesquisaProduto, GroupLayout.PREFERRED_SIZE, 438, GroupLayout.PREFERRED_SIZE)))
							.addGap(90)
							.addComponent(btPesquisaProduto, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
					.addGap(212))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap(17, Short.MAX_VALUE)
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addComponent(jLabel1)
						.addGroup(layout.createSequentialGroup()
							.addComponent(jtfDescricao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(jLabel5)
								.addComponent(jtfCodigo, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblValor)
								.addComponent(jtfValor, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(jcbAtivo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel2)
						.addComponent(ckAgrupa)
						.addComponent(ckDesconto))
					.addGap(18)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(jLabel8)
						.addComponent(tfPesquisaProduto, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
						.addComponent(btPesquisaProduto))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
					.addGap(106)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btCancelar)
						.addComponent(btExcluir)
						.addComponent(btAlterar)
						.addComponent(jbSalvar))
					.addContainerGap())
		);
		getContentPane().setLayout(layout);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void btPesquisaProdutoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btPesquisaProdutoActionPerformed
		// filtrar dados na tabela
		DefaultTableModel tabela = (DefaultTableModel) this.tbProdutos.getModel();
		final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tabela);
		this.tbProdutos.setRowSorter(sorter);
		String text = tfPesquisaProduto.getText();
		sorter.setRowFilter(RowFilter.regexFilter(text, 1, 2));
	}// GEN-LAST:event_btPesquisaProdutoActionPerformed

	private void btAlterarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btAlterarActionPerformed
		int linhaSelecionada = tbProdutos.getSelectedRow();
		// Verificamos se existe realmente alguma linha selecionada
		if (linhaSelecionada < 0) {
			JOptionPane.showMessageDialog(this, "Você deve selecionar um item na tabela antes de clicar no botão!", "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
		} else {
			//novoProduto();
			recuperarProduto();
			tipoCadastro = "alteracao";
		}
	}// GEN-LAST:event_btAlterarActionPerformed

	private void btExcluirActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btExcluirActionPerformed
		// excluir produto
		int linhaSelecionada = tbProdutos.getSelectedRow();
		// Verificamos se existe realmente alguma linha selecionada
		if (linhaSelecionada < 0) {
			JOptionPane.showMessageDialog(this, "Você deve selecionar um item na tabela antes de clicar no botão!", "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
		} else {
			this.excluirProduto();
		}
	}// GEN-LAST:event_btExcluirActionPerformed

	private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btCancelarActionPerformed
		novoProduto();
	}// GEN-LAST:event_btCancelarActionPerformed

	private void jbSalvarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jbSalvarActionPerformed

		if (tipoCadastro == null ||(tipoCadastro != null && "novo".equalsIgnoreCase(tipoCadastro))) {
			salvarProduto();
		} else {
			alterarProduto();
		}
		// if (modelProdutos.getCodigoBarrasEan() == null || modelProdutos.getCodigoBarrasEan().equals("0")) {
		// } else {
		// JOptionPane.showMessageDialog(this,
		// "Já existe um produto com este código de barras, \n"
		// + "Para cadastrar um produto sem código de barras digite 0!",
		// "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
		// }
		// } else if (tipoCadastro.equals("alteracao")) {
		// if (modelProdutos.getCodigoBarrasEan() == null || modelProdutos.getCodigoBarrasEan().equals("0")
		// || idDeBarras.equals(jtfIdBarras.getText())) {
		// alterarProduto();
		// } else {
		// JOptionPane.showMessageDialog(this,
		// "Já existe um produto com este código de barras, \n"
		// + "Para cadastrar um produto sem código de barras digite 0!",
		// "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
		// }
		// }
	}

	private void jtfDescricaoFocusLost(java.awt.event.FocusEvent evt) {
		// converte em maiusculo o que o usuario digitar
		this.jtfDescricao.setText(jtfDescricao.getText().toUpperCase());
		this.jtfDescricao.setText(bLMascaras.TiraAcentos(jtfDescricao.getText()));
	}

	private void tfPesquisaProdutoFocusLost(java.awt.event.FocusEvent evt) {
		// converte em maiusculo o que o usuario digitar
		this.tfPesquisaProduto.setText(tfPesquisaProduto.getText().toUpperCase());
		this.tfPesquisaProduto.setText(bLMascaras.TiraAcentos(tfPesquisaProduto.getText()));
	}

	private void excluirProduto() {
		int linha = tbProdutos.getSelectedRow();
		String descricao = (String) tbProdutos.getValueAt(linha, 2);
		int id = (Integer) tbProdutos.getValueAt(linha, 0);
		// pegunta se realmente deseja excluir
		int opcao = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja" + " excluir o registro \nDescricao: " + descricao + " ?", "Atenção", JOptionPane.YES_NO_OPTION);
		// se sim exclui, se não não faz nada
		if (opcao == JOptionPane.OK_OPTION) {
			if (controllerProdutos.excluirProdutosController(id)) {
				JOptionPane.showMessageDialog(this, "Registro excluido com suscesso!");
				carregarProdutos();
			} else {
				JOptionPane.showMessageDialog(this, "Você não pode excluir um produto que já foi vendido! \n" + "Para excluir o produto, delete primeiro a(s) venda(s) deste produto.", "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private boolean recuperarProduto() {
		// recebe a linha selecionada
		int linha = this.tbProdutos.getSelectedRow();
		// pega o id do cliente na linha selecionada
		int id = (Integer) tbProdutos.getValueAt(linha, 0);
		try {
			// recupera os dados do banco
			modelProdutos = controllerProdutos.getProdutosController(id);
			this.jtfCodigo.setText(String.valueOf(modelProdutos.getCodigo()));
			this.jtfDescricao.setText(modelProdutos.getDescricao());
			this.jtfValor.setText(modelProdutos.getValor().toString());
			this.jcbAtivo.setSelectedIndex(modelProdutos.getAtivo());
			this.ckAgrupa.setSelected(modelProdutos.isPodeAgrupar());
			this.ckDesconto.setSelected(modelProdutos.isPermiteDesconto());
			return true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Código inválido ou nenhum registro selecionado", "Aviso", JOptionPane.WARNING_MESSAGE);
			return false;
		}
	}

	private boolean alterarProduto() {
		modelProdutos.setDescricao(this.jtfDescricao.getText());
		modelProdutos.setValor(new BigDecimal(this.jtfValor.getText().replace(".", "").replace(",", ".")));
		modelProdutos.setAtivo(jcbAtivo.getSelectedIndex());
		modelProdutos.setPodeAgrupar(ckAgrupa.isSelected());
		modelProdutos.setPermiteDesconto(ckDesconto.isSelected());
		modelProdutos.setCodigo(this.jtfCodigo.getText());
		// alteracao
		if (controllerProdutos.atualizarProdutosController(modelProdutos)) {
			JOptionPane.showMessageDialog(this, "Registro alterado com sucesso!");
			this.carregarProdutos();
			novoProduto();
			return true;
		} else {
			JOptionPane.showMessageDialog(this, "Erro ao alterar os dados!", "ERRO", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	private void carregarProdutos() {
		listamModelProdutos = controllerProdutos.getListaProdutosController();
		DefaultTableModel modelo = (DefaultTableModel) tbProdutos.getModel();
		modelo.setNumRows(0);
		String ativo = "";
		// CARREGA OS DADOS DA LISTA NA TABELA
		int cont = listamModelProdutos.size();
		for (int i = 0; i < cont; i++) {
			if (listamModelProdutos.get(i).getAtivo() == 1) {
				ativo = "SIM";
			} else {
				ativo = "NAO";
			}
			modelo.addRow(new Object[] { listamModelProdutos.get(i).getId(), listamModelProdutos.get(i).getCodigo(), listamModelProdutos.get(i).getDescricao(), listamModelProdutos.get(i).getValor(), ativo, listamModelProdutos.get(i).isPodeAgrupar(),
					listamModelProdutos.get(i).isPermiteDesconto() });
		}
	}

	private void novoProduto() {
		jtfDescricao.setText("");
		jtfCodigo.setText("");
		jtfValor.setText("");
		jcbAtivo.setSelectedIndex(1);
		ckAgrupa.setSelected(true);
		tipoCadastro = "novo";
		modelProdutos = new ModelProdutos();
		
	}

	private boolean salvarProduto() {
		modelProdutos.setDescricao(this.jtfDescricao.getText());
		modelProdutos.setCodigo(this.jtfCodigo.getText());
		modelProdutos.setValor(new BigDecimal(jtfValor.getText().replace(".", "").replace(",", ".")));
		modelProdutos.setAtivo(jcbAtivo.getSelectedIndex());
		modelProdutos.setPodeAgrupar(ckAgrupa.isSelected());
		modelProdutos.setPermiteDesconto(ckDesconto.isSelected());
		// salvar
		if (controllerProdutos.salvarProdutosController(modelProdutos) > 0) {
			JOptionPane.showMessageDialog(this, "Registro gravado com sucesso!");
			this.carregarProdutos();
			novoProduto();
			return true;
		} else {
			JOptionPane.showMessageDialog(this, "Erro ao gravar os dados!", "ERRO", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new ViewProduto().setVisible(true);
			}
		});
	}

	public javax.swing.JTextField getJtfValor() {
		return jtfValor;
	}

	public void setJtfValor(javax.swing.JTextField jtfValor) {
		this.jtfValor = jtfValor;
	}

	private javax.swing.JButton btAlterar;
	private javax.swing.JButton btCancelar;
	private javax.swing.JButton btExcluir;
	private javax.swing.JButton btPesquisaProduto;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel8;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JButton jbSalvar;
	private javax.swing.JComboBox<String> jcbAtivo;
	private javax.swing.JTextField jtfDescricao;
	private javax.swing.JTextField jtfValor;
	private javax.swing.JTable tbProdutos;
	private javax.swing.JTextField tfPesquisaProduto;
	private JTextField jtfCodigo;
	private JCheckBox ckAgrupa;
	private JCheckBox ckDesconto;
}
