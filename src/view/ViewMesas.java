package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.ControllerCaixa;
import controller.ControllerFormaPagamento;
import controller.ControllerItensPedidoMesa;
import controller.ControllerProdutos;
import controller.ControllerUsuario;
import controller.ControllerVendas;
import javafx.scene.transform.Scale;
import model.ModelCaixa;
import model.ModelConfig;
import model.ModelItensPedidoMesa;
import model.ModelMesas;
import model.ModelProdutos;
import model.ModelSessaoUsuario;
import model.ModelUsuario;
import model.ModelVendas;
import util.BLMascaras;
import util.ImprimePDV;
import util.JMoneyField;
import util.ManipularXML;

public class ViewMesas extends javax.swing.JFrame {

	private static final long serialVersionUID = 1571073322214978183L;
	ArrayList<ModelProdutos> listaProdutoses = new ArrayList<>();
	ModelProdutos modelProdutos = new ModelProdutos();
	ControllerProdutos controllerProdutos = new ControllerProdutos();
	ModelItensPedidoMesa modelItensPedidoMesa = new ModelItensPedidoMesa();
	ControllerItensPedidoMesa controllerItensPedidoMesa = new ControllerItensPedidoMesa();
	ArrayList<ModelItensPedidoMesa> listaModelItensPedidoMesas = new ArrayList<>();
	ArrayList<ModelMesas> listaModelMesases = new ArrayList<>();
	ModelMesas modelMesas = new ModelMesas();
	ArrayList<ModelVendas> listaModelVendas = new ArrayList<>();
	ControllerVendas controllerVendas = new ControllerVendas();
	ModelCaixa modelCaixa = new ModelCaixa();
	ControllerCaixa controllerCaixa = new ControllerCaixa();
	ModelVendas modelVendas = new ModelVendas();
	ControllerFormaPagamento controllerFormaPagamento = new ControllerFormaPagamento();
	private ViewPagamento viewPagamento;
	private ViewGorjeta viewGorjeta;
	private ViewAlterStatus viewAlterStatus;
	public int codigoVenda;
	ArrayList<JRadioButton> listadeBotoes = new ArrayList<>();
	ArrayList<JButton> listadeBotoesNova = new ArrayList<>();
	int mesaParaImprimir;
	BLMascaras bLMascaras = new BLMascaras();
	private final int quantidadeMesa = ModelSessaoUsuario.quantidadeMesas;
	ArrayList<Integer> listaMesasOcupadas = new ArrayList<>();
	ModelConfig modelConfig = new ModelConfig();
	ManipularXML manipularXML = new ManipularXML();
	ControllerUsuario controllerUsuario = new ControllerUsuario();

	public ViewMesas() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		initComponents();
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		redimesionar();
		this.popularTabela();
		this.viewPagamento = new ViewPagamento(this, true);
		this.viewGorjeta = new ViewGorjeta(this, true);
		this.viewAlterStatus = new ViewAlterStatus(this, true);
		carregarProdutosList();
		carregarNumeroMesas();
	}

	private void redimesionar() {
		jPanelMesas.setMinimumSize(new Dimension(jPanel2.getWidth(), jPanel2.getHeight() - 30));
		jPanelMesas.setPreferredSize(new Dimension(jPanel2.getWidth(), jPanel2.getHeight() - 30));
		pack();
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {
		buttonGroup1 = new javax.swing.ButtonGroup();
		jTabbedPane1 = new javax.swing.JTabbedPane();
		jPanel2 = new javax.swing.JPanel();
		jPanelMesas = new javax.swing.JPanel();
		jPanel1 = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		jScrollPane2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		tbProdutos = new javax.swing.JTable();
		tbProdutos.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btRemover = new javax.swing.JButton();
		btRemover.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btFecharConta = new javax.swing.JButton();
		btFecharConta.setFont(new Font("Tahoma", Font.PLAIN, 11));
		tfValorTotalMesa = new JMoneyField(9);
		tfValorTotalMesa.setFont(new Font("Tahoma", Font.BOLD, 28));
		jLabel12 = new javax.swing.JLabel();
		jLabel12.setFont(new Font("Tahoma", Font.BOLD, 22));
		btVoltar = new javax.swing.JButton();
		btVoltar.setFont(new Font("Tahoma", Font.PLAIN, 11));
		jScrollPane3 = new javax.swing.JScrollPane();
		jScrollPane3.setFont(new Font("Tahoma", Font.PLAIN, 11));
		tbNomeProdutos = new javax.swing.JTable();
		tbNomeProdutos.setFont(new Font("Tahoma", Font.PLAIN, 11));
		jPanel5 = new javax.swing.JPanel();
		jLabel4 = new javax.swing.JLabel();
		jLabel4.setHorizontalAlignment(SwingConstants.CENTER);
		jlNumeroMesa = new javax.swing.JLabel();
		jlNumeroMesa.setHorizontalAlignment(SwingConstants.CENTER);
		jtfPesquisar = new javax.swing.JTextField();
		jtfPesquisar.setToolTipText("Pesquisar produto");
		jtfPesquisar.setFont(new Font("Tahoma", Font.PLAIN, 17));
		jtfPesquisar.grabFocus();
		jPanel4 = new javax.swing.JPanel();
		jcbNumeroMesa = new javax.swing.JComboBox<>();
		jLabel3 = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		jtProdutosStatus = new javax.swing.JTable();
		jLabel5 = new javax.swing.JLabel();
		jbVoltar = new javax.swing.JButton();
		jbAlterarStatus = new javax.swing.JButton();
		jMenuBar1 = new javax.swing.JMenuBar();
		jMenu1 = new javax.swing.JMenu();
		jmiConfigurar = new javax.swing.JMenuItem();
		jmiSair = new javax.swing.JMenuItem();
		jmiFecharConta = new javax.swing.JMenuItem();
		jmiRemoverProduto = new javax.swing.JMenuItem();
		jmiImprimirCupom = new javax.swing.JMenuItem();
		jmiGorjeta = new javax.swing.JMenuItem();
		jmiDesconto = new javax.swing.JMenuItem();
		tfDesconto = new JMoneyField(9);
		tfGorjeta = new JMoneyField(9);
		tfSubTotal = new JMoneyField(9);

		setTitle("Venda por Mesas");
		setIconImage(new ImageIcon(getClass().getResource("/imagens/blicon.png")).getImage());
		setUndecorated(false);

		jTabbedPane1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

		jPanelMesas.setBackground(new Color(204, 204, 204));

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		jPanel2Layout.setHorizontalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addComponent(jPanelMesas, GroupLayout.PREFERRED_SIZE, 960, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(398, Short.MAX_VALUE))
		);
		jPanel2Layout.setVerticalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanelMesas, GroupLayout.DEFAULT_SIZE, 632, Short.MAX_VALUE))
		);
		jPanel2.setLayout(jPanel2Layout);

		jTabbedPane1.addTab("Mesas", jPanel2);
		jTabbedPane1.setEnabledAt(0, false);

		jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder("ITENS DO PEDIDO"));

		tbProdutos.setBackground(new Color(255, 255, 205));
		tbProdutos.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "id", "Código", "Produto", "Quantidade.", "Valor Unit.", "Valor Total" }) {
			boolean[] canEdit = new boolean[] { false, false, false, false, false, false };

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		});
		jScrollPane2.setViewportView(tbProdutos);
		if (tbProdutos.getColumnModel().getColumnCount() > 0) {
			tbProdutos.getColumnModel().getColumn(0).setMinWidth(0);
			tbProdutos.getColumnModel().getColumn(0).setPreferredWidth(0);
			tbProdutos.getColumnModel().getColumn(0).setMaxWidth(0);
			tbProdutos.getColumnModel().getColumn(1).setResizable(true);
			tbProdutos.getColumnModel().getColumn(2).setResizable(true);
			tbProdutos.getColumnModel().getColumn(3).setResizable(true);
			tbProdutos.getColumnModel().getColumn(4).setResizable(true);
			
		}

		btRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 30/icons8-excluir-filled-50.png")));
		btRemover.setText("Remover Produto");
		btRemover.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btRemoverActionPerformed(evt);
			}
		});

		btFecharConta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 30/icons8-carrinho-de-compras-filled-50.png")));
		btFecharConta.setText("Fechar Conta");
		btFecharConta.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btFecharContaActionPerformed(evt);
			}
		});

		tfValorTotalMesa.setEditable(false);

		jLabel12.setText("Valor Total:");

		btVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 30/icons8-à-esquerda-dentro-de-um-círculo-filled-50.png")));
		btVoltar.setText("Voltar");
		btVoltar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btSalvarActionPerformed(evt);
				// btVoltarActionPerformed(evt);
			}
		});

		jScrollPane3.setBorder(javax.swing.BorderFactory.createTitledBorder("PRODUTOS"));

		tbNomeProdutos.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "Id", "Cod.", "Produto", "Valor", "podeAgrupar" }) {
			boolean[] canEdit = new boolean[] { false, false, false, false, false };

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		});
		tbNomeProdutos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		tbNomeProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tbNomeProdutosMouseClicked();
			}
		});
		jScrollPane3.setViewportView(tbNomeProdutos);
		if (tbNomeProdutos.getColumnModel().getColumnCount() > 0) {
			tbNomeProdutos.getColumnModel().getColumn(0).setMinWidth(0);
			tbNomeProdutos.getColumnModel().getColumn(0).setPreferredWidth(0);
			tbNomeProdutos.getColumnModel().getColumn(0).setMaxWidth(0);
			tbNomeProdutos.getColumnModel().getColumn(4).setMinWidth(0);
			tbNomeProdutos.getColumnModel().getColumn(4).setPreferredWidth(0);
			tbNomeProdutos.getColumnModel().getColumn(4).setMaxWidth(0);
		}

		jPanel5.setBackground(SystemColor.menu);

		jLabel4.setFont(new Font("Tahoma", Font.PLAIN, 38));
		jLabel4.setForeground(SystemColor.inactiveCaptionText);
		jLabel4.setText("MESA - ");

		jlNumeroMesa.setFont(new Font("Tahoma", Font.PLAIN, 38));
		jlNumeroMesa.setForeground(SystemColor.inactiveCaptionText);
		jlNumeroMesa.setText("nº");

		javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
		jPanel5Layout.setHorizontalGroup(
			jPanel5Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel5Layout.createSequentialGroup()
					.addGap(322)
					.addComponent(jLabel4)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(jlNumeroMesa)
					.addContainerGap(857, Short.MAX_VALUE))
		);
		jPanel5Layout.setVerticalGroup(
			jPanel5Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(jPanel5Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(jlNumeroMesa)
						.addComponent(jLabel4))
					.addContainerGap())
		);
		jPanel5.setLayout(jPanel5Layout);

		jtfPesquisar.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				jtfPesquisarKeyReleased(evt);
			}
		});
		jtfPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jtfQuantidade.grabFocus();
				jtfQuantidade.selectAll();
				tbNomeProdutos.setRowSelectionInterval(0, 0);
			}
		});

		JLabel lblNewLabel = new JLabel("Código ou Nome do Produto");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 17));

		jtfQuantidade = new JTextField();
		jtfQuantidade.setText("1");
		jtfQuantidade.setHorizontalAlignment(SwingConstants.CENTER);
		jtfQuantidade.setToolTipText("Quantidade");
		jtfQuantidade.setFont(new Font("Tahoma", Font.PLAIN, 17));
		jtfQuantidade.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (tbNomeProdutos.getRowCount() > 0) {
					int linha = 0;
					if (tbNomeProdutos.getSelectedRow() >= 0) {
						linha = tbNomeProdutos.getSelectedRow();
					}
					int id = (Integer) tbNomeProdutos.getValueAt(linha, 0);
					String codigo = (String) tbNomeProdutos.getValueAt(linha, 1);
					String nome = (String) tbNomeProdutos.getValueAt(linha, 2);
					BigDecimal valor = (BigDecimal) tbNomeProdutos.getValueAt(linha, 3);
					boolean podeAgrupar = (boolean) tbNomeProdutos.getValueAt(linha, 4);
					// viewQuantMesas.setVisible(true);
					setQuantidade(Float.parseFloat(jtfQuantidade.getText().replace(",", ".")));
					BigDecimal valorTotal = somaValorTotal(getQuantidade(), valor);
					// String observacao = viewQuantMesas.getObservacao();
					// Adiciona linhas na tabela
					adicionaProdutoAoPedido(id, codigo, nome, quantidade, valor, valorTotal, podeAgrupar);
					jtfQuantidade.setText("1");
					jtfPesquisar.setText("");
					jtfPesquisar.grabFocus();
				} else {

				}
			}
		});
		jtfQuantidade.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {
				if (!((e.getKeyChar() >= KeyEvent.VK_0 && e.getKeyChar() <= KeyEvent.VK_9)
						|| (e.getKeyChar() == KeyEvent.VK_ENTER || e.getKeyChar() == KeyEvent.VK_PERIOD || e.getKeyChar() == KeyEvent.VK_COMMA || e.getKeyChar() == KeyEvent.VK_BACK_SPACE))) {
					e.consume();
				}
			}

			public void keyPressed(KeyEvent e) {
			}

			public void keyReleased(KeyEvent e) {
			}
		});
		lblQuantidade = new JLabel();
		lblQuantidade.setHorizontalAlignment(SwingConstants.CENTER);
		lblQuantidade.setText("Quantidade");
		lblQuantidade.setFont(new Font("Tahoma", Font.BOLD, 17));

		JButton btImprimir = new JButton();
		btImprimir.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btImprimir.setIcon(new ImageIcon(ViewMesas.class.getResource("/imagens/icons 30/icons8-impressão-filled-50.png")));
		btImprimir.setText("Imprimir Cupom");
		btImprimir.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btImprimirActionPerformed(evt);
			}
		});

		JLabel lblGorjeta = new JLabel();
		lblGorjeta.setText("Gorjeta:");
		lblGorjeta.setFont(new Font("Tahoma", Font.PLAIN, 22));

		lblDesconto = new JLabel();
		lblDesconto.setText("Desconto:");
		lblDesconto.setFont(new Font("Tahoma", Font.PLAIN, 22));


		tfDesconto.setFont(new Font("Tahoma", Font.PLAIN, 22));
		tfDesconto.setEditable(false);
		tfGorjeta.setFont(new Font("Tahoma", Font.PLAIN, 22));
		tfGorjeta.setEditable(false);
		
		
		tfSubTotal.setFont(new Font("Tahoma", Font.BOLD, 22));
		tfSubTotal.setEditable(false);
		
		JLabel lbSubTotal = new JLabel();
		lbSubTotal.setText("Sub Total:");
		lbSubTotal.setFont(new Font("Tahoma", Font.BOLD, 22));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(jPanel5, GroupLayout.DEFAULT_SIZE, 1358, Short.MAX_VALUE)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGap(14)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(jtfPesquisar)
										.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addGap(47)
									.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
										.addComponent(jtfQuantidade)
										.addComponent(lblQuantidade)))
								.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(jScrollPane3, 0, 0, Short.MAX_VALUE)
									.addGroup(jPanel1Layout.createSequentialGroup()
										.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
											.addComponent(jLabel12, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING, false)
												.addGroup(jPanel1Layout.createSequentialGroup()
													.addComponent(lblGorjeta)
													.addGap(29))
												.addComponent(lblDesconto, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
											.addComponent(lbSubTotal, GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
											.addComponent(tfDesconto, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(tfValorTotalMesa, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(tfGorjeta, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(tfSubTotal, GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)))))
							.addGap(18)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addComponent(btVoltar, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btRemover)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btImprimir, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btFecharConta, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE))
								.addComponent(jScrollPane2))
							.addPreferredGap(ComponentPlacement.RELATED, 346, Short.MAX_VALUE)))
					.addGap(0))
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGap(7)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel)
								.addComponent(lblQuantidade))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(jtfPesquisar, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
								.addComponent(jtfQuantidade, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
							.addGap(10)
							.addComponent(jScrollPane3, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(tfSubTotal, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
								.addComponent(lbSubTotal, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addComponent(tfGorjeta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfDesconto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
									.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(lblGorjeta, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
										.addGap(45))
									.addComponent(lblDesconto, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)))
							.addGap(32))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(tfValorTotalMesa, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
								.addComponent(jLabel12, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
							.addGap(0))
						.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(btVoltar, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
							.addComponent(btRemover, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
							.addComponent(btImprimir, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
							.addComponent(btFecharConta, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		jPanel1.setLayout(jPanel1Layout);

		jTabbedPane1.addTab("Itens da mesa", jPanel1);
		jTabbedPane1.setEnabledAt(1, false);

		jcbNumeroMesa.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
		jcbNumeroMesa.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
			public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
			}

			public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
				jcbNumeroMesaPopupMenuWillBecomeInvisible(evt);
			}

			public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
			}
		});

		jLabel3.setText("Mesa");

		jtProdutosStatus.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "Status", "Nome", "Quant.", "Observação", "Cod item" }) {
			boolean[] canEdit = new boolean[] { false, false, false, false, false };

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		});
		jScrollPane1.setViewportView(jtProdutosStatus);
		if (jtProdutosStatus.getColumnModel().getColumnCount() > 0) {
			jtProdutosStatus.getColumnModel().getColumn(0).setPreferredWidth(150);
			jtProdutosStatus.getColumnModel().getColumn(2).setMinWidth(80);
			jtProdutosStatus.getColumnModel().getColumn(2).setPreferredWidth(80);
			jtProdutosStatus.getColumnModel().getColumn(2).setMaxWidth(80);
			jtProdutosStatus.getColumnModel().getColumn(4).setMinWidth(0);
			jtProdutosStatus.getColumnModel().getColumn(4).setPreferredWidth(0);
			jtProdutosStatus.getColumnModel().getColumn(4).setMaxWidth(0);
		}

		jLabel5.setText("Status dos itens do pedido");

		jbVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 30/icons8-à-esquerda-dentro-de-um-círculo-filled-50.png"))); // NOI18N
		jbVoltar.setText("Voltar");
		jbVoltar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbVoltarActionPerformed(evt);
			}
		});

		jbAlterarStatus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 30/icons8-editar-filled-50.png"))); // NOI18N
		jbAlterarStatus.setText("Alterar status");
		jbAlterarStatus.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbAlterarStatusActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout
				.setHorizontalGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						jPanel4Layout
								.createSequentialGroup().addContainerGap().addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addComponent(jcbNumeroMesa, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(javax.swing.GroupLayout.Alignment.LEADING,
												jPanel4Layout.createSequentialGroup().addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jLabel3).addComponent(jLabel5)).addGap(0, 0, Short.MAX_VALUE))
										.addGroup(jPanel4Layout.createSequentialGroup().addComponent(jbVoltar).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 629, Short.MAX_VALUE).addComponent(jbAlterarStatus)))
						.addContainerGap()));
		jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup().addContainerGap().addComponent(jLabel3).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jcbNumeroMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jLabel5)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jScrollPane1).addGap(18, 18, 18)
						.addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jbVoltar).addComponent(jbAlterarStatus)).addContainerGap()));
		jMenu1.setText("Arquivo");
		jMenu1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jMenu1ActionPerformed(evt);
			}
		});

		jmiConfigurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-manutenção-filled-50.png"))); // NOI18N
		jmiConfigurar.setText("Configurações");
		jmiConfigurar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jmiConfigurarActionPerformed(evt);
			}
		});
		jMenu1.add(jmiConfigurar);

		jmiSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icons 20/icons8-sair-filled-50.png"))); // NOI18N
		jmiSair.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
		jmiSair.setText("Sair");
		jmiSair.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				if (jTabbedPane1.getSelectedIndex() == 0) {
					jmiSairActionPerformed(evt);

				} else {
					jTabbedPane1.setSelectedIndex(0);
				}
			}
		});
		jMenu1.add(jmiSair);

		jMenuBar1.add(jMenu1);

		setJMenuBar(jMenuBar1);
		jmComandos = new javax.swing.JMenu();
		jmComandos.setText("Comandos");

		jmiFecharConta.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F8, 0));
		jmiFecharConta.setText("Fechar Conta");
		jmiFecharConta.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btFecharContaActionPerformed(evt);
			}
		});

		jmComandos.add(jmiFecharConta);

		jmiRemoverProduto.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
		jmiRemoverProduto.setText("Remover Produto");
		jmiRemoverProduto.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btRemoverActionPerformed(evt);
			}
		});
		jmComandos.add(jmiRemoverProduto);

		jmiImprimirCupom.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F11, 0));
		jmiImprimirCupom.setText("Imprimir Cupom");
		jmiImprimirCupom.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimirCupom();
			}
		});
		jmComandos.add(jmiImprimirCupom);

		jmiGorjeta.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, 0));
		jmiGorjeta.setText("Gorjeta");
		jmiGorjeta.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btGorjetaActionPerformed(evt);
			}
		});

		jmComandos.add(jmiGorjeta);

		jmiDesconto.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F3, 0));
		jmiDesconto.setText("Desconto");
		jmiDesconto.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btDescontoActionPerformed(evt);
			}
		});

		jmComandos.add(jmiDesconto);

		jMenuBar1.add(jmComandos);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(jTabbedPane1, GroupLayout.PREFERRED_SIZE, 1367, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(564, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(jTabbedPane1, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(334, Short.MAX_VALUE))
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void jbVizualizarMesaActionPerformed(java.awt.event.ActionEvent evt) {
		int mesa = Integer.parseInt(evt.getActionCommand().replaceAll("MESA ", ""));
		jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex() + 1);
		this.jlNumeroMesa.setText(mesa + "");
		// retornar todos os produtos do pedido da mesa
		listaModelItensPedidoMesas = controllerItensPedidoMesa.getListaItensPedidoMesaController(mesa);

		// preencher a tabela com os itens retornados
		DefaultTableModel modelo = (DefaultTableModel) tbProdutos.getModel();
		modelo.setNumRows(0);
		// CARREGA OS DADOS DA LISTA NA TABELA
		int cont = listaModelItensPedidoMesas.size();
		for (int i = 0; i < cont; i++) {
			modelProdutos = controllerProdutos.getProdutosController(listaModelItensPedidoMesas.get(i).getCodigoProduto());
			modelo.addRow(new Object[] { modelProdutos.getId(), listaModelItensPedidoMesas.get(i).getCodigoProduto(), modelProdutos.getDescricao(), listaModelItensPedidoMesas.get(i).getQuantidade(), modelProdutos.getValor(),
					(modelProdutos.getValor().multiply(new BigDecimal(Float.toString(listaModelItensPedidoMesas.get(i).getQuantidade())))).setScale(2, RoundingMode.HALF_EVEN) });
		}
		if(!listaModelItensPedidoMesas.isEmpty()) {
			codigoVenda = listaModelItensPedidoMesas.iterator().next().getCodigoVenda();
		}else {
			codigoVenda = 0;
		}
		// SOMA VALOR TOTAL
		somaEAtualizaValorTotal();
		jtfPesquisar.grabFocus();
	}

	private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {
		modelMesas = new ModelMesas();
		// cadastra venda
		salvarVenda();
		this.cadastrarVendaMesa();
		//salvar venda
		// setar os dados da mesa
		modelMesas.setNumeroMesa(Integer.parseInt(this.jlNumeroMesa.getText()));
		modelMesas.setSituacaoMesa("ocupada");
		popularTabela();
		// limpar campos
		this.limparCamposAbaDois();
		this.limparTabelaAbaDois();
	}

	private void btFecharContaActionPerformed(java.awt.event.ActionEvent evt) {
		modelMesas = new ModelMesas();
		// fechar conta da mesa

		// setar os dados da mesa
		modelMesas.setNumeroMesa(Integer.parseInt(this.jlNumeroMesa.getText()));
		modelMesas.setSituacaoMesa("livre");

		viewPagamento.setValorTotal(new BigDecimal(this.tfValorTotalMesa.getText().replace(",", ".")));
		viewPagamento.setTextFildValorTotal();
		// viewPagamentoPDV.setVisible(true);

		if (salvarVenda()) {
			viewPagamento.setCodigoVenda(codigoVenda);
			viewPagamento.setVisible(true);
		}
		// exlui dados anteriores
		controllerItensPedidoMesa.excluirItensPedidoMesaController(Integer.parseInt(this.jlNumeroMesa.getText()));
		// pintar na tela
		popularTabela();
		// limpar dados da interface
		this.limparCamposAbaDois();
		this.limparTabelaAbaDois();
		jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex() - 1);
	}

	private void btGorjetaActionPerformed(ActionEvent evt) {
		String percentual = JOptionPane.showInputDialog(this, "Informe o valor", "Gorjeta", 3);
		try {
			setPercentualGorjeta(Float.parseFloat(percentual.replace(",", ".")));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Valor incorreta!", "ERRO", JOptionPane.ERROR_MESSAGE);
		}
		somaEAtualizaValorTotal();
		// viewGorjeta.setValorTotal(new BigDecimal(this.tfValorTotalMesa.getText().replace(",", ".")));
		// viewGorjeta.setVisible(true);
		// setValorGorjeta(viewGorjeta.getValorGorjeta());
		// tfGorjeta.setText(getValorGorjeta().toString());
	}

	private void btDescontoActionPerformed(ActionEvent evt) {
		String percentual = JOptionPane.showInputDialog(this, "Informe o valor", "Desconto", 3);
		try {
			setPercentualGorjeta(Float.parseFloat(percentual.replace(",", ".")));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Valor incorreta!", "ERRO", JOptionPane.ERROR_MESSAGE);
		}
		somaEAtualizaValorTotal();
		// viewGorjeta.setValorTotal(new BigDecimal(this.tfValorTotalMesa.getText().replace(",", ".")));
		// viewGorjeta.setVisible(true);
		// setValorGorjeta(viewGorjeta.getValorGorjeta());
		// tfGorjeta.setText(getValorGorjeta().toString());
	}

	private void btRemoverActionPerformed(ActionEvent evt) {
		// excluir linha produto
		this.excluirLinhaProduto();
		somaEAtualizaValorTotal();
	}

	private void btVoltarActionPerformed(ActionEvent evt) {
		jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex() - 1);
		salvarVenda();
	}
	
	private void btImprimirActionPerformed(java.awt.event.ActionEvent evt) {
		//salvar venda
		salvarVenda();
		imprimirCupom();
	}


	private void tbNomeProdutosMouseClicked() {// GEN-FIRST:event_tbNomeProdutosMouseClicked
		jtfQuantidade.grabFocus();
		jtfQuantidade.selectAll();
	}

	private void adicionaProdutoAoPedido(int id, String codigo, String nome, float quantidade, BigDecimal valor, BigDecimal valorTotal, boolean podeAgrupar) {
		DefaultTableModel modelo = (DefaultTableModel) tbProdutos.getModel();
		if (podeAgrupar) {
			boolean encontrou = false;
			for (int i = 0; i < modelo.getRowCount(); i++) {
				if (Integer.parseInt(modelo.getValueAt(i, 0).toString()) == id) {
					Float novaQuantidade = (float) modelo.getValueAt(i, 3) + quantidade;
					BigDecimal novoTotal = new BigDecimal(novaQuantidade.toString()).multiply(valor).setScale(2, RoundingMode.HALF_EVEN);
					modelo.setValueAt(novaQuantidade, i, 3);
					modelo.setValueAt(novoTotal, i, 5);
					encontrou = true;
					break;
				}
			}

			if (!encontrou) {
				modelo.addRow(new Object[] { id, codigo, nome, quantidade, valor, valorTotal });
			}
		} else {
			modelo.addRow(new Object[] { id, codigo, nome, quantidade, valor, valorTotal });
		}
		somaEAtualizaValorTotal();
	}

	private void jcbNumeroMesaPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {

		if (this.jcbNumeroMesa.isPopupVisible()) {
			carregarItensStatusMesa();
		}
	}

	private void jbAlterarStatusActionPerformed(java.awt.event.ActionEvent evt) {
		int linha = jtProdutosStatus.getSelectedRow();
		// Verificamos se existe realmente alguma linha selecionada
		if (linha < 0) {
			JOptionPane.showMessageDialog(this, "Você deve selecionar um item na tabela antes de clicar aqui!", "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
		} else {
			String status = (String) jtProdutosStatus.getValueAt(linha, 0);
			int item = (int) jtProdutosStatus.getValueAt(linha, 4);
			// setar dados na proxima interface
			viewAlterStatus.setStatus(status);
			viewAlterStatus.setItem(item);
			viewAlterStatus.setTextFildStatus();
			// chamr interface
			viewAlterStatus.setVisible(true);
			// atualiza a tabela
			carregarItensStatusMesa();
		}
		//
	}

	private void jbVoltarActionPerformed(java.awt.event.ActionEvent evt) {
		jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex() - 2);
	}

	private void jtfPesquisarKeyReleased(java.awt.event.KeyEvent evt) {
		// filtrar na tabela quando digitar
		this.jtfPesquisar.setText(jtfPesquisar.getText().toUpperCase());
		DefaultTableModel tabelaProdutos = (DefaultTableModel) this.tbNomeProdutos.getModel();
		final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tabelaProdutos);
		this.tbNomeProdutos.setRowSorter(sorter);
		String text = jtfPesquisar.getText();
		
		List<RowFilter<Object, Object>> filters = new ArrayList<RowFilter<Object, Object>>(2);
		filters.add(0,RowFilter.regexFilter("(?i)^" + text + "$", 1));
		if(text.contains("^[a-Z]")) {
			filters.add(1,RowFilter.regexFilter("(?i)" + text, 2));
		}
		sorter.setRowFilter(RowFilter.orFilter(filters));
	}

	private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {

	}

	private void jmiConfigurarActionPerformed(java.awt.event.ActionEvent evt) {

		int quantMesas = 0;
		try {
			quantMesas = Integer.parseInt(JOptionPane.showInputDialog("Qual é o seu nome?"));
			salarArquivoXML(quantMesas);
			JOptionPane.showMessageDialog(this, "Inicia novamente a venda por mesa!");
			dispose();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Você deve informar um número inteiro!");
			quantMesas = 50;
		}
	}

	private void jmiSairActionPerformed(java.awt.event.ActionEvent evt) {
		dispose();
	}

	private void salarArquivoXML(int pquantMesas) {
		modelConfig = new ModelConfig();
		modelConfig = manipularXML.lerXml3();
		try {
			modelConfig.setQuantidadeMesas(pquantMesas);
			ModelSessaoUsuario.quantidadeMesas = modelConfig.getQuantidadeMesas();
			manipularXML.gravaXML(modelConfig);
			JOptionPane.showMessageDialog(this, "Registro salvo com sucesso!", "Atenção", JOptionPane.WARNING_MESSAGE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Imprimir cupom
	 */
	private void imprimirCupomCozinha(int codigo) {
		ImprimePDV imprimePDV = new ImprimePDV();
		try {
			imprimePDV.geraCupomCozinha(codigo);
		} catch (IOException ex) {
			Logger.getLogger(ViewPdv.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void carregarItensStatusMesa() {
		int mesa = jcbNumeroMesa.getSelectedIndex() + 1;
		// retornar todos os produtos do pedido da mesa
		listaModelItensPedidoMesas = controllerItensPedidoMesa.getListaItensPedidoMesaController(mesa);

		// preencher a tabela com os itens retornados
		DefaultTableModel modelo = (DefaultTableModel) jtProdutosStatus.getModel();
		modelo.setNumRows(0);
		// CARREGA OS DADOS DA LISTA NA TABELA
		int cont = listaModelItensPedidoMesas.size();
		for (int i = 0; i < cont; i++) {
			modelProdutos = controllerProdutos.getProdutosController(listaModelItensPedidoMesas.get(i).getCodigoProduto());
			modelo.addRow(new Object[] { listaModelItensPedidoMesas.get(i).getStatusPedido(), modelProdutos.getDescricao(), listaModelItensPedidoMesas.get(i).getQuantidade(), listaModelItensPedidoMesas.get(i).getCodigo() });
		}
	}

	public void centralizarContainer(Component pai, Component filho) {

		// obter dimensões do pai
		int larguraPai = pai.getWidth();
		int alturaPai = pai.getHeight();

		// obter dimensões do filho
		int larguraFilho = filho.getWidth();
		int alturaFilho = filho.getWidth();

		// calcular novas coordenadas do filho
		int novoX = (larguraPai - larguraFilho);
		int novoY = (alturaPai - alturaFilho);

		// centralizar filho
		filho.setSize(new Dimension(novoX, novoY));
		filho.repaint();
	}

	// Excluir linha produto selecionado
	private void excluirLinhaProduto() {
		int linhaSelecionada = tbProdutos.getSelectedRow();
		if (linhaSelecionada < 0) {
			return;
		} else {
			String senha = JOptionPane.showInputDialog(this, "Informe a senha", "Remover Item", 3);
			if (senha != null) {
				ModelUsuario usuario = controllerUsuario.getUsuarioController(ModelSessaoUsuario.codigo);
				if (senha.equals(usuario.getSenhaCancelamento())) {
					DefaultTableModel modelo = (DefaultTableModel) tbProdutos.getModel();
					modelo.removeRow(linhaSelecionada);
				} else {
					JOptionPane.showMessageDialog(this, "Senha incorreta!", "ERRO", JOptionPane.ERROR_MESSAGE);
					excluirLinhaProduto();
				}
			}
		}
	}

	/**
	 * inserir e pentar mesas.
	 */
	private void popularTabela() {
		jPanelMesas.removeAll();
		jPanelMesas.validate();
		listaMesasOcupadas = controllerItensPedidoMesa.getListaMesasOcupadasController();
		for (int i = 1; i <= quantidadeMesa; i++) {
			JLabel labelDesenho = new JLabel();
			JRadioButton btRadio = new JRadioButton();
			JButton mesa = new JButton();
			// labelDesenho.setIcon(new
			// javax.swing.ImageIcon(getClass().getResource("/imagens/mesa.jpg")));
			labelDesenho.setText("        ");

			btRadio.setFont(new Font("Tahoma", 0, 40));
			mesa.setFont(new Font("Tahoma", 0, 60));
			mesa.setForeground(new Color(95, 158, 160));
			for (int j = 0; j < listaMesasOcupadas.size(); j++) {
				if (i == listaMesasOcupadas.get(j)) {
					mesa.setForeground(new Color(128, 0, 0));
					break;
				}
			}
			if (i < 10) {
				mesa.setText("MESA 0" + i);
			} else {
				mesa.setText("MESA " + i);
			}

			btRadio.setText("" + i);
			btRadio.setVisible(false);
			buttonGroup1.add(btRadio);
			listadeBotoes.add(btRadio);
			// adicionar no painel
			jPanelMesas.add(btRadio);
			jPanelMesas.add(mesa);
			mesa.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {

					jbVizualizarMesaActionPerformed(evt);

				}
			});
			labelDesenho.setLabelFor(btRadio);
		}
	}

	// salvar uma venda de mesa
	private boolean salvarVenda() {
		listaModelVendas = new ArrayList<>();
		listaProdutoses = new ArrayList<>();
		int idProduto;
		quantidade = 0;

		for (int i = 0; i < tbProdutos.getRowCount(); i++) {
			modelVendas = new ModelVendas();
			modelProdutos = new ModelProdutos();
			modelVendas.setValorTotal(new BigDecimal(this.tfValorTotalMesa.getText().replace(".", "").replace(",", ".")));
			modelVendas.setCodigoUsuario(ModelSessaoUsuario.codigo);
			try {
				modelVendas.setDataVenda(bLMascaras.converterDataParaDateUS(new java.util.Date(System.currentTimeMillis())));
			} catch (Exception ex) {
				Logger.getLogger(ex.getMessage());
			}
			idProduto = Integer.parseInt(tbProdutos.getValueAt(i, 0).toString());
			modelVendas.setProdutosCodigo(idProduto);
			modelVendas.setQuantidade(Float.parseFloat(tbProdutos.getValueAt(i, 3).toString()));
			modelVendas.setValorGorjeta(getValorGorjeta());
			modelProdutos.setId(idProduto);
			modelProdutos.setValor(new BigDecimal(tbProdutos.getValueAt(i, 5).toString()));
			listaModelVendas.add(modelVendas);
			listaProdutoses.add(modelProdutos);
		}
		modelVendas.setListamModelVendases(listaModelVendas);
		modelProdutos.setListaModelProdutoses(listaProdutoses);

		// salvar venda
		if(codigoVenda == 0) {
			codigoVenda = controllerVendas.salvarVendasController(modelVendas);
			return true;
		}else if (codigoVenda > 0) {
			modelVendas.setCodigo(codigoVenda);
			// salvar lista de produtos
			controllerVendas.atualizarVendasController(modelVendas);
			// JOptionPane.showMessageDialog(this, "Registro gravado com sucesso!");
			// imprimir cupom
			// imprimirCupom();

			return true;
		} 
		return false;
	}

	/**
	 * Imprimir cupom
	 */
	private void imprimirCupom() {
		ImprimePDV imprimePDV = new ImprimePDV();
		try {
			imprimePDV.geraCupomTXT(codigoVenda);
		} catch (IOException ex) {
			Logger.getLogger(ViewPdv.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	// soma valor total da conta do cliente e seta no campo total
	private BigDecimal somaValorTotal(float pQuantidade, BigDecimal pValor) {
		return pValor.multiply(new BigDecimal(Float.toString(pQuantidade))).setScale(2, RoundingMode.HALF_EVEN);
	}

	// limpar dados da tabela da aba 2
	private void limparTabelaAbaDois() {
		DefaultTableModel modelo = (DefaultTableModel) tbProdutos.getModel();
		modelo.setNumRows(0);
	}

	// limpar campos da tabela da aba 2
	private void limparCamposAbaDois() {
		this.tfValorTotalMesa.setText("");
		this.tfGorjeta.setText("");
		this.tfDesconto.setText("");
		this.percentualGorjeta = 0F;
		this.tfSubTotal.setText("");
	}

	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
		// (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel. For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(ViewMesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(ViewMesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(ViewMesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(ViewMesas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		// </editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new ViewMesas().setVisible(true);
			}
		});
	}

	// listar todos os produtos no jlist
	private void carregarProdutosList() {
		DefaultTableModel modelo = (DefaultTableModel) tbNomeProdutos.getModel();
		modelo.setNumRows(0);
		listaProdutoses = controllerProdutos.getListaProdutosController();
		for (int i = 0; i < listaProdutoses.size(); i++) {
			modelo.addRow(new Object[] { listaProdutoses.get(i).getId(), listaProdutoses.get(i).getCodigo(), listaProdutoses.get(i).getDescricao(), listaProdutoses.get(i).getValor(), listaProdutoses.get(i).isPodeAgrupar() });
		}

	}

	private void carregarNumeroMesas() {
		jcbNumeroMesa.removeAllItems();
		for (int i = 0; i < quantidadeMesa; i++) {
			jcbNumeroMesa.addItem("Mesa " + (i + 1));
		}
		carregarItensStatusMesa();
	}

	// cadastrar um venda de uma mesa
	private boolean cadastrarVendaMesa() {
		int numeroMesa = Integer.parseInt(this.jlNumeroMesa.getText());

		// exlui dados anteriores
		controllerItensPedidoMesa.excluirItensPedidoMesaController(numeroMesa);
		if (tbProdutos.getRowCount() < 1) {
			jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex() - 1);
			// JOptionPane.showMessageDialog(this, "Você deve adicionar ao menos um produto
			// para salvar!", "ATENÇÂO", JOptionPane.WARNING_MESSAGE);
			return false;
		} else {
			listaModelItensPedidoMesas = new ArrayList<>();

			for (int i = 0; i < tbProdutos.getRowCount(); i++) {
				modelItensPedidoMesa = new ModelItensPedidoMesa();
				modelItensPedidoMesa.setCodigoMesa(numeroMesa);
				modelItensPedidoMesa.setCodigoProduto(Integer.parseInt(tbProdutos.getValueAt(i, 0).toString()));
				modelItensPedidoMesa.setQuantidade(Float.parseFloat(tbProdutos.getValueAt(i, 3).toString()));
				modelItensPedidoMesa.setValorUnitario(new BigDecimal(tbProdutos.getValueAt(i, 4).toString()).setScale(2, RoundingMode.HALF_EVEN ));

				modelItensPedidoMesa.setCodigoVenda(codigoVenda);
				modelItensPedidoMesa.setHora(bLMascaras.retornarHora());
				listaModelItensPedidoMesas.add(modelItensPedidoMesa);
			}

			modelProdutos.setListaModelProdutoses(listaProdutoses);

			// exclui registros anteriores se ouver
			controllerItensPedidoMesa.excluirItensPedidoMesaController(modelItensPedidoMesa.getCodigoMesa());
			// salvar
			controllerItensPedidoMesa.salvarItensPedidoMesaController(listaModelItensPedidoMesas);
			// JOptionPane.showMessageDialog(this, "Registro gravado com sucesso!");
			// atualizar
			// volta para aba 1
			jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex() - 1);
			return true;
		}
	}

	/**
	 * Soma e atualiza os valores total
	 *
	 * @return
	 */
	private void somaEAtualizaValorTotal() {
		BigDecimal subTotal = BigDecimal.ZERO;
		BigDecimal valorItem = BigDecimal.ZERO;
		BigDecimal gorjeta = BigDecimal.ZERO;
		int cont = tbProdutos.getRowCount();
		for (int i = 0; i < cont; i++) {
			valorItem = new BigDecimal(String.valueOf(tbProdutos.getValueAt(i, 5)));
			subTotal = subTotal.add(valorItem);
		}
		if(percentualGorjeta > 0) {
			BigDecimal percentualBigDecimal = new BigDecimal(String.valueOf(percentualGorjeta / 100));
			gorjeta = percentualBigDecimal.multiply(subTotal).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		}
		this.tfSubTotal.setText(String.valueOf(subTotal));
		this.tfValorTotalMesa.setText(String.valueOf(subTotal.add(gorjeta)));
		setValorGorjeta(gorjeta);
		this.tfGorjeta.setText(String.valueOf(gorjeta));
	}
	
	public float getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton btFecharConta;
	private javax.swing.JButton btRemover;
	private javax.swing.JButton btVoltar;
	private javax.swing.ButtonGroup buttonGroup1;
	private javax.swing.JLabel jLabel12;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JMenu jMenu1;
	private javax.swing.JMenuBar jMenuBar1;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JPanel jPanel5;
	private javax.swing.JPanel jPanelMesas;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JButton jbAlterarStatus;
	private javax.swing.JButton jbVoltar;
	private javax.swing.JComboBox<String> jcbNumeroMesa;
	private javax.swing.JLabel jlNumeroMesa;
	private javax.swing.JMenuItem jmiConfigurar;
	private javax.swing.JMenuItem jmiSair;
	private javax.swing.JTable jtProdutosStatus;
	private javax.swing.JTextField jtfPesquisar;
	private javax.swing.JTable tbNomeProdutos;
	private javax.swing.JTable tbProdutos;
	private javax.swing.JTextField tfValorTotalMesa;
	private JTextField jtfQuantidade;
	private JLabel lblQuantidade;
	private float quantidade;
	private javax.swing.JMenu jmComandos;
	private javax.swing.JMenuItem jmiFecharConta;
	private javax.swing.JMenuItem jmiRemoverProduto;
	private javax.swing.JMenuItem jmiImprimirCupom;
	private javax.swing.JMenuItem jmiGorjeta;
	private javax.swing.JMenuItem jmiDesconto;
	private BigDecimal valorGorjeta = BigDecimal.ZERO;
	private JLabel lblDesconto;
	private JTextField tfDesconto;
	private JTextField tfGorjeta;
	private JTextField tfSubTotal;
	private Float percentualGorjeta = 0F;

	public BigDecimal getValorGorjeta() {
		return valorGorjeta;
	}

	public void setValorGorjeta(BigDecimal valorGorjeta) {
		this.valorGorjeta = valorGorjeta;
	}

	public Float getPercentualGorjeta() {
		return percentualGorjeta;
	}

	public void setPercentualGorjeta(Float percentualGorjeta) {
		this.percentualGorjeta = percentualGorjeta;
	}
}
