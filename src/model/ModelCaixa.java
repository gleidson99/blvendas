package model;

import java.math.BigDecimal;
import java.sql.Date;

public class ModelCaixa {

	private int codigo;
	private BigDecimal dinheiro;
	private BigDecimal cheque;
	private BigDecimal cartao;
	private BigDecimal convenio;
	private int caixaNumero;
	private int codigoUsuario;
	private int status;
	private Date dataAbertura;
	private Date dataFechamento;

	public ModelCaixa() {
	}

	public void setCodigo(int pCodigo) {
		this.codigo = pCodigo;
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setDinheiro(BigDecimal pDinheiro) {
		this.dinheiro = pDinheiro;
	}

	public BigDecimal getDinheiro() {
		return this.dinheiro;
	}

	public void setCheque(BigDecimal pCheque) {
		this.cheque = pCheque;
	}

	public BigDecimal getCheque() {
		return this.cheque;
	}

	public void setCartao(BigDecimal pCartao) {
		this.cartao = pCartao;
	}

	public BigDecimal getCartao() {
		return this.cartao;
	}

	public void setConvenio(BigDecimal pVale) {
		this.convenio = pVale;
	}

	public BigDecimal getConvenio() {
		return this.convenio;
	}

	@Override
	public String toString() {
		return "ModelCaixa {" + "::codigo = " + this.codigo + "::dinheiro = " + this.dinheiro + "::cheque = "
				+ this.cheque + "::cartao = " + this.cartao + "::vale = " + this.convenio + "}";
	}

	public int getCaixaNumero() {
		return caixaNumero;
	}

	public void setCaixaNumero(int caixaNumero) {
		this.caixaNumero = caixaNumero;
	}

	public int getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(int codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public Date getDataFechamento() {
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}
}