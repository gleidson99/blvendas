package model;

public class ModelGarcom {

    private int codigo;
    private String nome;
    private String endereco;
    private String bairro;
    private int codCidade;
    private int codEstado;
    private String cep;
    private String telefone;
    private float comissao;
    private String senha;

    public ModelGarcom(){}

    public void setCodigo(int pCodigo){
        this.codigo = pCodigo;
    }

    public int getCodigo(){
        return this.codigo;
    }

    public void setNome(String pNome){
        this.nome = pNome;
    }

    public String getNome(){
        return this.nome;
    }

    public void setEndereco(String pEndereco){
        this.endereco = pEndereco;
    }

    public String getEndereco(){
        return this.endereco;
    }

    public void setBairro(String pBairro){
        this.bairro = pBairro;
    }

    public String getBairro(){
        return this.bairro;
    }

    public void setCodCidade(int pCodCidade){
        this.codCidade = pCodCidade;
    }

    public int getCodCidade(){
        return this.codCidade;
    }

    public void setCodEstado(int pCodEstado){
        this.codEstado = pCodEstado;
    }

    public int getCodEstado(){
        return this.codEstado;
    }

    public void setCep(String pCep){
        this.cep = pCep;
    }

    public String getCep(){
        return this.cep;
    }

    public void setTelefone(String pTelefone){
        this.telefone = pTelefone;
    }

    public String getTelefone(){
        return this.telefone;
    }

    public void setComissao(float pComissao){
        this.comissao = pComissao;
    }

    public float getComissao(){
        return this.comissao;
    }

    public void setSenha(String pSenha){
        this.senha = pSenha;
    }

    public String getSenha(){
        return this.senha;
    }

    @Override
    public String toString(){
        return "ModelGarcom {" + "::codigo = " + this.codigo + "::nome = " + this.nome + "::endereco = " + this.endereco + "::bairro = " + this.bairro + "::codCidade = " + this.codCidade + "::codEstado = " + this.codEstado + "::cep = " + this.cep + "::telefone = " + this.telefone + "::comissao = " + this.comissao + "::senha = " + this.senha +  "}";
    }
}