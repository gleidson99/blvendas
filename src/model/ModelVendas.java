package model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;

public class ModelVendas {

	private int codigo;
	private int produtosCodigo;
	private Date dataVenda;
	private float quantidade;
	private BigDecimal valorTotal;
	private BigDecimal valorDesconto = BigDecimal.ZERO;
	private BigDecimal valorGorjeta = BigDecimal.ZERO;
	private ArrayList<ModelVendas> listamModelVendases;
	private int codigoUsuario;
	private Float percentualGorjeta = 0F;

	public ModelVendas() {
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getProdutosCodigo() {
		return produtosCodigo;
	}

	public void setProdutosCodigo(int produtosCodigo) {
		this.produtosCodigo = produtosCodigo;
	}

	public Date getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}

	public float getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public ArrayList<ModelVendas> getListamModelVendases() {
		return listamModelVendases;
	}

	public void setListamModelVendases(ArrayList<ModelVendas> listamModelVendases) {
		this.listamModelVendases = listamModelVendases;
	}

	public int getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(int codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	@Override
	public String toString() {
		return "ModelVendas {" + "::codigo = " + this.codigo + "::produtosCodigo = " + this.produtosCodigo + "::dataVenda = " + this.dataVenda + "::quantidade = " + this.quantidade + "}";
	}

	public Float getPercentualGorjeta() {
		return percentualGorjeta;
	}

	public void setPercentualGorjeta(Float percentualGorjeta) {
		this.percentualGorjeta = percentualGorjeta;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public BigDecimal getValorGorjeta() {
		return valorGorjeta;
	}

	public void setValorGorjeta(BigDecimal valorGorjeta) {
		this.valorGorjeta = valorGorjeta;
	}

}