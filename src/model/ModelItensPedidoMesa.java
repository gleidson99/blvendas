package model;

import java.math.BigDecimal;

public class ModelItensPedidoMesa {

    private int codigo;
    private int codigoMesa;
    private int codigoProduto;
    private String statusPedido;
    private float quantidade;
    private String hora;
    private int codigoVenda;
    private BigDecimal valorUnitario;

    public ModelItensPedidoMesa(){}

    public void setCodigo(int pCodigo){
        this.codigo = pCodigo;
    }

    public int getCodigo(){
        return this.codigo;
    }

    public void setCodigoMesa(int pCodigoMesa){
        this.codigoMesa = pCodigoMesa;
    }

    public int getCodigoMesa(){
        return this.codigoMesa;
    }

    public void setCodigoProduto(int pCodigoProduto){
        this.codigoProduto = pCodigoProduto;
    }

    public int getCodigoProduto(){
        return this.codigoProduto;
    }

    public void setStatusPedido(String pStatusPedido){
        this.statusPedido = pStatusPedido;
    }

    public String getStatusPedido(){
        return this.statusPedido;
    }

    @Override
    public String toString(){
		return "ModelItensPedidoMesa {" + "::codigo = " + this.codigo + "::codigoMesa = " + this.codigoMesa + "::codigoProduto = " + this.codigoProduto + "::statusPedido = " + this.statusPedido + "}";
    }

    public float getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(float quantidade) {
        this.quantidade = quantidade;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

	public int getCodigoVenda() {
		return codigoVenda;
	}

	public void setCodigoVenda(int codigoVenda) {
		this.codigoVenda = codigoVenda;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
    
}