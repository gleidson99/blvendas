package model;

import java.math.BigDecimal;

public class ModelPagamento {

    private int codigo;
	private int fk_venda;
	private int fk_forma_pagamento;
	private BigDecimal valor;

    public ModelPagamento(){}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getFk_venda() {
		return fk_venda;
	}

	public void setFk_venda(int fk_venda) {
		this.fk_venda = fk_venda;
	}

	public int getFk_forma_pagamento() {
		return fk_forma_pagamento;
	}

	public void setFk_forma_pagamento(int fk_forma_pagamento) {
		this.fk_forma_pagamento = fk_forma_pagamento;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
    public String toString(){
		return "ModelPagamento {" + "::codigo = " + this.codigo + "::fk_forma_pagamento = " + this.fk_forma_pagamento + "::fk_venda = " + this.fk_venda + "::valor = " + this.valor + "}";
    }

}