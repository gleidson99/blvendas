package model;

public class ModelMesas {

    private int codigo;
    private int numeroMesa;
    private String situacaoMesa;

    public ModelMesas(){}

    public void setCodigo(int pCodigo){
        this.codigo = pCodigo;
    }

    public int getCodigo(){
        return this.codigo;
    }

    public void setNumeroMesa(int pNumeroMesa){
        this.numeroMesa = pNumeroMesa;
    }
    
    public int getNumeroMesa(){
        return this.numeroMesa;
    }

    public void setSituacaoMesa(String pSituacaoMesa){
        this.situacaoMesa = pSituacaoMesa;
    }

    public String getSituacaoMesa(){
        return this.situacaoMesa;
    }

    @Override
    public String toString(){
        return "ModelMesas {" + "::codigo = " + this.codigo + "::numeroMesa = " + this.numeroMesa + "::situacaoMesa = " + this.situacaoMesa +  "}";
    }

}