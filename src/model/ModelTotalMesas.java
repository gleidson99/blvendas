package model;

public class ModelTotalMesas {

    private int codigo;
    private int quantidade;

    public ModelTotalMesas(){}

    public void setCodigo(int pCodigo){
        this.codigo = pCodigo;
    }

    public int getCodigo(){
        return this.codigo;
    }

    public void setQuantidade(int pQuantidade){
        this.quantidade = pQuantidade;
    }

    public int getQuantidade(){
        return this.quantidade;
    }

    @Override
    public String toString(){
        return "ModelTotalMesas {" + "::codigo = " + this.codigo + "::quantidade = " + this.quantidade +  "}";
    }
}