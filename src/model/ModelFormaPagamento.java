package model;

public class ModelFormaPagamento {

    private int codigo;
    private String descricao;
    private boolean situacao;

    public ModelFormaPagamento(){}

    public void setCodigo(int pCodigo){
        this.codigo = pCodigo;
    }

    public int getCodigo(){
        return this.codigo;
    }

    public void setDescricao(String pDescricao){
        this.descricao = pDescricao;
    }

    public String getDescricao(){
        return this.descricao;
    }

    public void setSituacao(boolean pSituacao){
        this.situacao = pSituacao;
    }

    public boolean isSituacao(){
        return this.situacao;
    }

    @Override
    public String toString(){
		return this.descricao;
    }
}