package model;

import java.sql.Date;

public class ModelCompras {

    private int codigo;
    private double valorTotal;
    private Date data;

    public ModelCompras(){}

    public void setCodigo(int pCodigo){
        this.codigo = pCodigo;
    }

    public int getCodigo(){
        return this.codigo;
    }

    public void setValorTotal(double pValorTotal){
        this.valorTotal = pValorTotal;
    }

    public double getValorTotal(){
        return this.valorTotal;
    }

    @Override
    public String toString(){
        return "ModelCompras {" + "::codigo = " + this.codigo + "::valorTotal = " + this.valorTotal +  "}";
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}