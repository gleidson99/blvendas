package model;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ModelProdutos {

	private int id;
	private String codigo;
	private String descricao;
	private BigDecimal valor;
	private ArrayList<ModelProdutos> listaModelProdutoses;
	private int ativo;
	private boolean podeAgrupar;
	private boolean permiteDesconto;

	public ModelProdutos() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public ArrayList<ModelProdutos> getListaModelProdutoses() {
		return listaModelProdutoses;
	}

	public void setListaModelProdutoses(ArrayList<ModelProdutos> listaModelProdutoses) {
		this.listaModelProdutoses = listaModelProdutoses;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public boolean isPodeAgrupar() {
		return podeAgrupar;
	}

	public void setPodeAgrupar(boolean podeAgrupar) {
		this.podeAgrupar = podeAgrupar;
	}

	public boolean isPermiteDesconto() {
		return permiteDesconto;
	}

	public void setPermiteDesconto(boolean permiteDesconto) {
		this.permiteDesconto = permiteDesconto;
	}

}