/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

public class ModelConfig {

    public String getCaminhoMySQL() {
        return caminhoMySQL;
    }

    public void setCaminhoMySQL(String caminhoMySQL) {
        this.caminhoMySQL = caminhoMySQL;
    }
    
    private String nomeBanco;
    private String usuario;
    private String senha;
    private String ip;
    private String caminhoMySQL;
    private int quantidadeMesas;

    public String getNomeBanco() {
        return nomeBanco;
    }

    public void setNomeBanco(String nomeBanco) {
        this.nomeBanco = nomeBanco;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getQuantidadeMesas() {
        return quantidadeMesas;
	}

	public void setQuantidadeMesas(int quantidadeMesas) {
        this.quantidadeMesas = quantidadeMesas;
	}
}
