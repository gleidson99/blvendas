package DAO;

import java.util.ArrayList;

import conexoes.ConexaoMySql;
import model.ModelFormaPagamento;

public class DAOFormaPagamento extends ConexaoMySql {

    public int salvarFormaPagamentoDAO(ModelFormaPagamento pModelFormaPagamento) {
        try {
            this.conectar();
            int situacao = 0;
            if (pModelFormaPagamento.isSituacao()) {
                situacao = 1;
            } else {
                situacao = 0;
            }
            return this.insertSQL(
                    "INSERT INTO forma_pagamento ("
                    + "descricao,"
                    + "situacao"
                    + ") VALUES ("
                    + "'" + pModelFormaPagamento.getDescricao() + "',"
                    + "'" + situacao + "'"
                    + ");"
            );
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            this.fecharConexao();
        }
    }

    public ModelFormaPagamento getFormaPagamentoDAO(int pCodigo){
        ModelFormaPagamento modelFormaPagamento = new ModelFormaPagamento();
        try {
            this.conectar();
            this.executarSQL(
                "SELECT "
                    + "codigo,"
                    + "descricao,"
                    + "situacao"
                 + " FROM"
                     + " forma_pagamento"
                 + " WHERE"
                     + " codigo = '" + pCodigo + "'"
                + ";"
            );

            while(this.getResultSet().next()){
                modelFormaPagamento.setCodigo(this.getResultSet().getInt(1));
                modelFormaPagamento.setDescricao(this.getResultSet().getString(2));
                modelFormaPagamento.setSituacao(this.getResultSet().getBoolean(3));
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            this.fecharConexao();
        }
        return modelFormaPagamento;
    }

    public ModelFormaPagamento getFormaPagamentoDAO(String pDescricao){
        ModelFormaPagamento modelFormaPagamento = new ModelFormaPagamento();
        try {
            this.conectar();
            this.executarSQL(
                "SELECT "
                    + "codigo,"
                    + "descricao,"
                    + "situacao"
                 + " FROM"
                     + " forma_pagamento"
                 + " WHERE"
                     + " descricao = '" + pDescricao + "'"
                + ";"
            );

            while(this.getResultSet().next()){
                modelFormaPagamento.setCodigo(this.getResultSet().getInt(1));
                modelFormaPagamento.setDescricao(this.getResultSet().getString(2));
                modelFormaPagamento.setSituacao(this.getResultSet().getBoolean(3));
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            this.fecharConexao();
        }
        return modelFormaPagamento;
    }

    public ArrayList<ModelFormaPagamento> getListaFormaPagamentoDAO(){
        ArrayList<ModelFormaPagamento> listamodelFormaPagamento = new ArrayList();
        ModelFormaPagamento modelFormaPagamento = new ModelFormaPagamento();
        try {
            this.conectar();
            this.executarSQL(
					"SELECT codigo, descricao, situacao FROM forma_pagamento;");

            while(this.getResultSet().next()){
                modelFormaPagamento = new ModelFormaPagamento();
                modelFormaPagamento.setCodigo(this.getResultSet().getInt(1));
                modelFormaPagamento.setDescricao(this.getResultSet().getString(2));
                modelFormaPagamento.setSituacao(this.getResultSet().getBoolean(3));
                listamodelFormaPagamento.add(modelFormaPagamento);
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            this.fecharConexao();
        }
        return listamodelFormaPagamento;
    }

    public boolean atualizarFormaPagamentoDAO(ModelFormaPagamento pModelFormaPagamento){
        try {
            this.conectar();
            int situacao = 0;
            if (pModelFormaPagamento.isSituacao()) {
                situacao = 1;
            } else {
                situacao = 0;
            }
            return this.executarUpdateDeleteSQL(
                "UPDATE forma_pagamento SET "
                    + "codigo = '" + pModelFormaPagamento.getCodigo() + "',"
                    + "descricao = '" + pModelFormaPagamento.getDescricao() + "',"
                    + "situacao = '" + situacao + "'"
                + " WHERE "
                    + "codigo = '" + pModelFormaPagamento.getCodigo() + "'"
                + ";"
            );
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.fecharConexao();
        }
    }

    public boolean excluirFormaPagamentoDAO(int pCodigo){
        try {
            this.conectar();
            return this.executarUpdateDeleteSQL(
                "DELETE FROM forma_pagamento "
                + " WHERE "
                    + "codigo = '" + pCodigo + "'"
                + ";"
            );
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.fecharConexao();
        }
    }
}