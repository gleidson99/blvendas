package DAO;

import java.util.ArrayList;

import conexoes.ConexaoMySql;
import model.ModelItensPedidoMesa;

public class DAOItensPedidoMesa extends ConexaoMySql {

    public int salvarItensPedidoMesaDAO(ModelItensPedidoMesa pModelItensPedidoMesa) {
        try {
            this.conectar();
            return this.insertSQL(
                    "INSERT INTO itens_pedido_mesa ("
                    + "codigo,"
                    + "codigo_mesa,"
                    + "codigo_produto,"
                    + "status_pedido,"
                    + "quantidade, "
                    + "codigo_venda, "
                    + "valor_unitario "
                    + ") VALUES ("
                    + "'" + pModelItensPedidoMesa.getCodigo() + "',"
                    + "'" + pModelItensPedidoMesa.getCodigoMesa() + "',"
                    + "'" + pModelItensPedidoMesa.getCodigoProduto() + "',"
                    + "'" + pModelItensPedidoMesa.getStatusPedido() + "',"
                    + "'" + pModelItensPedidoMesa.getQuantidade() + "',"
                    + "'" + pModelItensPedidoMesa.getCodigoVenda() + "',"
                    + "'" + pModelItensPedidoMesa.getValorUnitario() + "'"
                    + ");"
            );
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            this.fecharConexao();
        }
    }

    public ModelItensPedidoMesa getItensPedidoMesaDAO(int pCodigo) {
        ModelItensPedidoMesa modelItensPedidoMesa = new ModelItensPedidoMesa();
        try {
            this.conectar();
            this.executarSQL(
                    "SELECT "
                    + "codigo,"
                    + "codigo_mesa,"
                    + "codigo_produto,"
                    + "status_pedido,"
                    + "quantidade,"
                    + "codigo_venda,"
                    + "valor_unitario"
                    + " FROM"
                    + " itens_pedido_mesa"
                    + " WHERE"
                    + " codigo = '" + pCodigo + "'"
                    + ";"
            );

            while (this.getResultSet().next()) {
                modelItensPedidoMesa.setCodigo(this.getResultSet().getInt(1));
                modelItensPedidoMesa.setCodigoMesa(this.getResultSet().getInt(2));
                modelItensPedidoMesa.setCodigoProduto(this.getResultSet().getInt(3));
                modelItensPedidoMesa.setStatusPedido(this.getResultSet().getString(4));
				modelItensPedidoMesa.setQuantidade(this.getResultSet().getFloat(5));
				modelItensPedidoMesa.setCodigoVenda(this.getResultSet().getInt(6));
				modelItensPedidoMesa.setValorUnitario(this.getResultSet().getBigDecimal(7));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.fecharConexao();
        }
        return modelItensPedidoMesa;
    }

    public ArrayList<ModelItensPedidoMesa> getListaItensPedidoMesaDAO() {
        ArrayList<ModelItensPedidoMesa> listamodelItensPedidoMesa = new ArrayList();
        ModelItensPedidoMesa modelItensPedidoMesa = new ModelItensPedidoMesa();
        try {
            this.conectar();
            this.executarSQL(
                    "SELECT "
                    + "codigo,"
                    + "codigo_mesa,"
                    + "codigo_produto,"
                    + "status_pedido,"
                    + "quantidade "
                    + "codigo_venda "
                    + "valor_unitario, "
                    + " FROM"
                    + " itens_pedido_mesa"
                    + ";"
            );

            while (this.getResultSet().next()) {
                modelItensPedidoMesa = new ModelItensPedidoMesa();
                modelItensPedidoMesa.setCodigo(this.getResultSet().getInt(1));
                modelItensPedidoMesa.setCodigoMesa(this.getResultSet().getInt(2));
                modelItensPedidoMesa.setCodigoProduto(this.getResultSet().getInt(3));
                modelItensPedidoMesa.setStatusPedido(this.getResultSet().getString(4));
				modelItensPedidoMesa.setQuantidade(this.getResultSet().getFloat(5));
				modelItensPedidoMesa.setCodigoVenda(this.getResultSet().getInt(6));
				modelItensPedidoMesa.setValorUnitario(this.getResultSet().getBigDecimal(7));
                listamodelItensPedidoMesa.add(modelItensPedidoMesa);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.fecharConexao();
        }
        return listamodelItensPedidoMesa;
    }

    public ArrayList<Integer> getListaMesasOcupadasDAO() {
        ArrayList<Integer> listaNumeroMesas = new ArrayList<>();
        try {
            this.conectar();
            this.executarSQL(
                    "SELECT DISTINCT "
                    + "codigo_mesa "
                    + " FROM"
                    + " itens_pedido_mesa"
                    + ";"
            );

            while (this.getResultSet().next()) {
                listaNumeroMesas.add(this.getResultSet().getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.fecharConexao();
        }
        return listaNumeroMesas;
    }

    public ArrayList<ModelItensPedidoMesa> getListaItensPedidoMesaCozinhaDAO() {
        ArrayList<ModelItensPedidoMesa> listamodelItensPedidoMesa = new ArrayList();
        ModelItensPedidoMesa modelItensPedidoMesa = new ModelItensPedidoMesa();
        try {
            this.conectar();
            this.executarSQL(
                    "SELECT "
                    + "codigo,"
                    + "codigo_mesa,"
                    + "codigo_produto,"
                    + "status_pedido,"
                    + "quantidade "
                    + "codigo_venda "
                    + "valor_unitario, "
                    + " FROM"
                    + " itens_pedido_mesa "
                    + "WHERE status_pedido != 'Entregue'"
                    + ";"
            );

            while (this.getResultSet().next()) {
                modelItensPedidoMesa = new ModelItensPedidoMesa();
                modelItensPedidoMesa.setCodigo(this.getResultSet().getInt(1));
                modelItensPedidoMesa.setCodigoMesa(this.getResultSet().getInt(2));
                modelItensPedidoMesa.setCodigoProduto(this.getResultSet().getInt(3));
                modelItensPedidoMesa.setStatusPedido(this.getResultSet().getString(4));
				modelItensPedidoMesa.setQuantidade(this.getResultSet().getFloat(5));
				modelItensPedidoMesa.setCodigoVenda(this.getResultSet().getInt(6));
				modelItensPedidoMesa.setValorUnitario(this.getResultSet().getBigDecimal(7));
                listamodelItensPedidoMesa.add(modelItensPedidoMesa);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.fecharConexao();
        }
        return listamodelItensPedidoMesa;
    }

    public ArrayList<ModelItensPedidoMesa> getListaItensPedidoMesaDAO(int pCodigo) {
        ArrayList<ModelItensPedidoMesa> listamodelItensPedidoMesa = new ArrayList();
        ModelItensPedidoMesa modelItensPedidoMesa = new ModelItensPedidoMesa();
        try {
            this.conectar();
            this.executarSQL(
                    "SELECT itens_pedido_mesa.codigo, "
                    + "codigo_mesa, "
                    + "codigo_produto, "
                    + "status_pedido, "
                    + "quantidade, "
                    + "codigo_venda, "
                    + "valor_unitario "
                    + "FROM itens_pedido_mesa "
                    + "where codigo_mesa = '" + pCodigo + "'"
            );

            while (this.getResultSet().next()) {
                modelItensPedidoMesa = new ModelItensPedidoMesa();
                modelItensPedidoMesa.setCodigo(this.getResultSet().getInt(1));
                modelItensPedidoMesa.setCodigoMesa(this.getResultSet().getInt(2));
                modelItensPedidoMesa.setCodigoProduto(this.getResultSet().getInt(3));
                modelItensPedidoMesa.setStatusPedido(this.getResultSet().getString(4));
				modelItensPedidoMesa.setQuantidade(this.getResultSet().getFloat(5));
				modelItensPedidoMesa.setCodigoVenda(this.getResultSet().getInt(6));
				modelItensPedidoMesa.setValorUnitario(this.getResultSet().getBigDecimal(7));
                listamodelItensPedidoMesa.add(modelItensPedidoMesa);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.fecharConexao();
        }
        return listamodelItensPedidoMesa;
    }
    
    public ArrayList<ModelItensPedidoMesa> getListaItensPedidoPorVenda(int codigoVenda) {
        ArrayList<ModelItensPedidoMesa> listamodelItensPedidoMesa = new ArrayList();
        ModelItensPedidoMesa modelItensPedidoMesa = new ModelItensPedidoMesa();
        try {
            this.conectar();
            this.executarSQL(
                    "SELECT itens_pedido_mesa.codigo, "
                    + "codigo_mesa, "
                    + "codigo_produto, "
                    + "status_pedido, "
                    + "quantidade, "
                    + "codigo_venda, "
                    + "valor_unitario "
                    + "FROM itens_pedido_mesa "
                    + "where codigo_venda = '" + codigoVenda + "'"
            );

            while (this.getResultSet().next()) {
                modelItensPedidoMesa = new ModelItensPedidoMesa();
                modelItensPedidoMesa.setCodigo(this.getResultSet().getInt(1));
                modelItensPedidoMesa.setCodigoMesa(this.getResultSet().getInt(2));
                modelItensPedidoMesa.setCodigoProduto(this.getResultSet().getInt(3));
                modelItensPedidoMesa.setStatusPedido(this.getResultSet().getString(4));
				modelItensPedidoMesa.setQuantidade(this.getResultSet().getFloat(5));
				modelItensPedidoMesa.setCodigoVenda(this.getResultSet().getInt(6));
				modelItensPedidoMesa.setValorUnitario(this.getResultSet().getBigDecimal(7));
                listamodelItensPedidoMesa.add(modelItensPedidoMesa);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.fecharConexao();
        }
        return listamodelItensPedidoMesa;
    }

    public boolean atualizarItensPedidoMesaDAO(ModelItensPedidoMesa pModelItensPedidoMesa) {
        try {
            this.conectar();
            return this.executarUpdateDeleteSQL(
                    "UPDATE itens_pedido_mesa SET "
                    + "codigo = '" + pModelItensPedidoMesa.getCodigo() + "',"
                    + "codigo_mesa = '" + pModelItensPedidoMesa.getCodigoMesa() + "',"
                    + "codigo_produto = '" + pModelItensPedidoMesa.getCodigoProduto() + "',"
                    + "status_pedido = '" + pModelItensPedidoMesa.getStatusPedido() + "',"
                    + "quantidade = '" + pModelItensPedidoMesa.getQuantidade() + "',"
                    + "codigo_venda = '" + pModelItensPedidoMesa.getCodigoVenda() + "',"
                    + "valor_unitario = '" + pModelItensPedidoMesa.getValorUnitario() + "'"
                    + " WHERE "
                    + "codigo = '" + pModelItensPedidoMesa.getCodigo() + "'"
                    + ";"
            );
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            this.fecharConexao();
        }
    }

    public boolean atualizarStatusItemDAO(ModelItensPedidoMesa pModelItensPedidoMesa) {
        try {
            this.conectar();
            return this.executarUpdateDeleteSQL(
                    "UPDATE itens_pedido_mesa SET "
                    + "status_pedido = '" + pModelItensPedidoMesa.getStatusPedido() + "'"
                    + " WHERE "
                    + "codigo = '" + pModelItensPedidoMesa.getCodigo() + "'"
                    + ";"
            );
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            this.fecharConexao();
        }
    }

    public boolean excluirItensPedidoMesaDAO(int pCodigo) {
        try {
            this.conectar();
            return this.executarUpdateDeleteSQL(
                    "DELETE FROM itens_pedido_mesa "
                    + " WHERE "
                    + "codigo_mesa = '" + pCodigo + "'"
                    + ";"
            );
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            this.fecharConexao();
        }
    }

    public boolean salvarItensPedidoMesaDAO(ArrayList<ModelItensPedidoMesa> pListaModelItensPedidoMesas) {
        try {
            this.conectar();
            int sizeLista = pListaModelItensPedidoMesas.size();
            for (int i = 0; i < sizeLista; i++) {
                this.insertSQL(
                        "INSERT INTO itens_pedido_mesa ("
                        + "codigo_mesa,"
                        + "codigo_produto,"
                        + "quantidade, "
                        + "valor_unitario, "
                        + "codigo_venda, "
                        + "horario "
                        + ") VALUES ("
                        + "'" + pListaModelItensPedidoMesas.get(i).getCodigoMesa() + "',"
                        + "'" + pListaModelItensPedidoMesas.get(i).getCodigoProduto() + "',"
                        + "'" + pListaModelItensPedidoMesas.get(i).getQuantidade() + "',"
                        + "'" + pListaModelItensPedidoMesas.get(i).getValorUnitario() + "',"
                        + "'" + pListaModelItensPedidoMesas.get(i).getCodigoVenda() + "',"
                        + "'" + pListaModelItensPedidoMesas.get(i).getHora() + "'"
                        + ");"
                );
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            this.fecharConexao();
        }
    }
}
