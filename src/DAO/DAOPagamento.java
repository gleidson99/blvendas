package DAO;

import java.util.ArrayList;

import conexoes.ConexaoMySql;
import model.ModelPagamento;

public class DAOPagamento extends ConexaoMySql {

	public int salvarPagamentoDAO(ModelPagamento pModelPagamento) {
        try {
            this.conectar();
            return this.insertSQL(
					"INSERT INTO pagamento (" + "valor," + "codigo_forma_pagamento," + "codigo_venda"
                    + ") VALUES ("
							+ "'" + pModelPagamento.getValor() + "'," + "'" + pModelPagamento.getFk_forma_pagamento() + "'," + "'" + pModelPagamento.getFk_venda() + "'"
                    + ");"
            );
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            this.fecharConexao();
        }
    }

	public ModelPagamento getPagamentoDAO(int pCodigo) {
		ModelPagamento modelPagamento = new ModelPagamento();
        try {
            this.conectar();
            this.executarSQL(
                "SELECT "
                    + "codigo,"
							+ "valor," + "codigo_forma_pagamento," + "codigo_venda"
                 + " FROM"
							+ " pagamento"
                 + " WHERE"
                     + " codigo = '" + pCodigo + "'"
                + ";"
            );
			while (this.getResultSet().next()) {
				modelPagamento.setCodigo(this.getResultSet().getInt(1));
				modelPagamento.setValor(this.getResultSet().getBigDecimal(2));
				modelPagamento.setFk_forma_pagamento(this.getResultSet().getInt(3));
				modelPagamento.setFk_venda(this.getResultSet().getInt(4));

            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            this.fecharConexao();
        }
		return modelPagamento;
    }
    
	public ArrayList<ModelPagamento> getListaPagamentoDAO() {
		ArrayList<ModelPagamento> listamodelPagamento = new ArrayList();
		ModelPagamento modelPagamento = new ModelPagamento();
        try {
            this.conectar();
            this.executarSQL(
					"SELECT codigo, descricao, desconto, quantidade_parcelas, observacao, situacao FROM pagamento;");
            while(this.getResultSet().next()){
				modelPagamento = new ModelPagamento();
				modelPagamento.setCodigo(this.getResultSet().getInt(1));
				modelPagamento.setValor(this.getResultSet().getBigDecimal(2));
				modelPagamento.setFk_forma_pagamento(this.getResultSet().getInt(3));
				modelPagamento.setFk_venda(this.getResultSet().getInt(4));
				listamodelPagamento.add(modelPagamento);
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            this.fecharConexao();
        }
		return listamodelPagamento;
    }

    /**
	 * atualiza Pagamento
	 * 
	 * @param pModelPagamento
	 *            return boolean
	 */
	public boolean atualizarPagamentoDAO(ModelPagamento pModelPagamento) {
        try {
            this.conectar();
            return this.executarUpdateDeleteSQL(
					"UPDATE pagamento SET " + "codigo = '" + pModelPagamento.getCodigo() + "'," + "valor = '" + pModelPagamento.getValor() + "'," + "codigo_forma_pagamento = '" + pModelPagamento.getFk_forma_pagamento() + "'," + "codigo_venda = '"
							+ pModelPagamento.getFk_venda() + "'"
                + " WHERE "
							+ "codigo = '" + pModelPagamento.getCodigo() + "'"
                + ";"
            );
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.fecharConexao();
        }
    }

    /**
	 * exclui Pagamento
	 * 
	 * @param pCodigo
	 *            return boolean
	 */
	public boolean excluirPagamentoDAO(int pCodigo) {
        try {
            this.conectar();
            return this.executarUpdateDeleteSQL(
					"DELETE FROM pagamento "
                + " WHERE "
                    + "codigo = '" + pCodigo + "'"
                + ";"
            );
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            this.fecharConexao();
        }
    }
}