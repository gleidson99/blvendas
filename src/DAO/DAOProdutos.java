package DAO;

import java.util.ArrayList;

import conexoes.ConexaoMySql;
import model.ModelProdutos;

public class DAOProdutos extends ConexaoMySql {

	public int salvarProdutosDAO(ModelProdutos pModelProdutos) {
		try {
			this.conectar();
			return this.insertSQL("INSERT INTO produtos (codigo, descricao, valor, ativo, pode_agrupar, permite_desconto) VALUES (" + "'" + pModelProdutos.getCodigo() + "'," + "'" + pModelProdutos.getDescricao() + "'," + "'"
					+ pModelProdutos.getValor() + "'," + "'" + pModelProdutos.getAtivo() + "'," +  pModelProdutos.isPodeAgrupar() + "," + pModelProdutos.isPermiteDesconto() + ");");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			this.fecharConexao();
		}
	}

	public ModelProdutos getProdutosDAO(int pId) {
		ModelProdutos modelProdutos = new ModelProdutos();
		try {
			this.conectar();
			this.executarSQL("SELECT id, codigo, descricao, valor, ativo, pode_agrupar, permite_desconto FROM produtos WHERE id = '" + pId + "'" + ";");

			while (this.getResultSet().next()) {
				modelProdutos.setId(this.getResultSet().getInt(1));
				modelProdutos.setCodigo(this.getResultSet().getString(2));
				modelProdutos.setDescricao(this.getResultSet().getString(3));
				modelProdutos.setValor(this.getResultSet().getBigDecimal(4));
				modelProdutos.setAtivo(this.getResultSet().getInt(5));
				modelProdutos.setPodeAgrupar(this.getResultSet().getBoolean(6));
				modelProdutos.setPermiteDesconto(this.getResultSet().getBoolean(7));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return modelProdutos;
	}

	public ModelProdutos getProdutosDAO(String pDescricao) {
		ModelProdutos modelProdutos = new ModelProdutos();
		try {
			this.conectar();
			this.executarSQL("SELECT id, codigo, descricao, valor, ativo, pode_agrupar, permite_desconto FROM produtos WHERE descricao = '" + pDescricao + "'" + ";");

			while (this.getResultSet().next()) {
				modelProdutos.setId(this.getResultSet().getInt(1));
				modelProdutos.setCodigo(this.getResultSet().getString(2));
				modelProdutos.setDescricao(this.getResultSet().getString(3));
				modelProdutos.setValor(this.getResultSet().getBigDecimal(4));
				modelProdutos.setAtivo(this.getResultSet().getInt(5));
				modelProdutos.setPodeAgrupar(this.getResultSet().getBoolean(6));
				modelProdutos.setPermiteDesconto(this.getResultSet().getBoolean(7));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return modelProdutos;
	}

	public ArrayList<ModelProdutos> getListaProdutosDAO() {
		ArrayList<ModelProdutos> listamodelProdutos = new ArrayList();
		ModelProdutos modelProdutos = new ModelProdutos();
		try {
			this.conectar();
			this.executarSQL("SELECT id, codigo, descricao, valor, ativo, pode_agrupar, permite_desconto FROM produtos" + ";");

			while (this.getResultSet().next()) {
				modelProdutos = new ModelProdutos();
				modelProdutos.setId(this.getResultSet().getInt(1));
				modelProdutos.setCodigo(this.getResultSet().getString(2));
				modelProdutos.setDescricao(this.getResultSet().getString(3));
				modelProdutos.setValor(this.getResultSet().getBigDecimal(4));
				modelProdutos.setAtivo(this.getResultSet().getInt(5));
				modelProdutos.setPodeAgrupar(this.getResultSet().getBoolean(6));
				modelProdutos.setPermiteDesconto(this.getResultSet().getBoolean(7));
				listamodelProdutos.add(modelProdutos);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return listamodelProdutos;
	}

	public ArrayList<ModelProdutos> getListaProdutosAtivosDAO() {
		ArrayList<ModelProdutos> listamodelProdutos = new ArrayList();
		ModelProdutos modelProdutos = new ModelProdutos();
		try {
			this.conectar();
			this.executarSQL("SELECT id, codigo, descricao, valor, ativo, pode_agrupar, permite_desconto FROM produtos WHERE ativo = 1;");

			while (this.getResultSet().next()) {
				modelProdutos = new ModelProdutos();
				modelProdutos.setId(this.getResultSet().getInt(1));
				modelProdutos.setCodigo(this.getResultSet().getString(2));
				modelProdutos.setDescricao(this.getResultSet().getString(3));
				modelProdutos.setValor(this.getResultSet().getBigDecimal(4));
				modelProdutos.setAtivo(this.getResultSet().getInt(5));
				modelProdutos.setPodeAgrupar(this.getResultSet().getBoolean(6));
				modelProdutos.setPermiteDesconto(this.getResultSet().getBoolean(7));
				listamodelProdutos.add(modelProdutos);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return listamodelProdutos;
	}

	public boolean atualizarProdutosDAO(ModelProdutos pModelProdutos) {
		try {
			this.conectar();
			this.executarUpdateDeleteSQL("UPDATE produtos SET " + "codigo = '" + pModelProdutos.getCodigo() + "', " + "descricao = '" + pModelProdutos.getDescricao() + "', " + "valor = '" + pModelProdutos.getValor() + "', " + "ativo = '"
					+ pModelProdutos.getAtivo() + "', " + "pode_agrupar = " + pModelProdutos.isPodeAgrupar() + ", " + "permite_desconto = " + pModelProdutos.isPermiteDesconto() + " WHERE id = " + pModelProdutos.getId() + ";");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			this.fecharConexao();
		}
	}

	public boolean excluirProdutosDAO(int pId) {
		try {
			this.conectar();
			return this.executarUpdateDeleteSQL("DELETE FROM produtos " + " WHERE " + "id = '" + pId + "'" + ";");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Tentativa de violar uma chave");
			return false;
		} finally {
			this.fecharConexao();
		}
	}
}
