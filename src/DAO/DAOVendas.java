package DAO;

import java.util.ArrayList;

import conexoes.ConexaoMySql;
import model.ModelVendas;

public class DAOVendas extends ConexaoMySql {

	public int salvarVendasDAO(ModelVendas pModelVendas) {
		try {
			this.conectar();
			return this.insertSQL("INSERT INTO vendas (data_venda, valor_total, desconto, codigo_usuario, gorjeta) VALUES ("
					+ "'"+ pModelVendas.getDataVenda() + "'," 
					+ "'" + pModelVendas.getValorTotal() + "'," 
					+ "'" + pModelVendas.getValorDesconto() + "',"
					+ "'" + pModelVendas.getCodigoUsuario() + "'," 
					+ "'" + pModelVendas.getValorGorjeta() + "'"
					+ ");");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			this.fecharConexao();
		}
	}

	public boolean salvarProdutosVendasDAO(ModelVendas pModelVendas) {
		try {
			this.conectar();
			int sizeLista = pModelVendas.getListamModelVendases().size();
			for (int i = 0; i < sizeLista; i++) {
				this.insertSQL("insert into VENDAS_PRODUTO( " + "CODIGO_PRODUTO," + "CODIGO_VENDA," + "QUANTIDADE, "
						+ "VALOR_UNITARIO) " + " VALUES (" + "'"
						+ pModelVendas.getListamModelVendases().get(i).getProdutosCodigo() + "'," + "'"
						+ pModelVendas.getCodigo() + "'," + "'"
						+ pModelVendas.getListamModelVendases().get(i).getQuantidade() + "'," + ");");
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			this.fecharConexao();
		}
	}

	public ModelVendas getVendasDAO(int pCodigo) {
		ModelVendas modelVendas = new ModelVendas();
		try {
			this.conectar();
			this.executarSQL("SELECT codigo, valor_total, data_venda, desconto, codigo_usuario, gorjeta  FROM vendas WHERE" + " codigo = '" + pCodigo + "'" + ";");

			while (this.getResultSet().next()) {
				modelVendas.setCodigo(this.getResultSet().getInt(1));
				modelVendas.setValorTotal(this.getResultSet().getBigDecimal(2));
				modelVendas.setDataVenda(this.getResultSet().getDate(3));
				modelVendas.setValorDesconto(this.getResultSet().getBigDecimal(4));
				modelVendas.setCodigoUsuario(this.getResultSet().getInt(5));
				modelVendas.setValorGorjeta(this.getResultSet().getBigDecimal(6));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return modelVendas;
	}

	public ArrayList<ModelVendas> getListaPedidosDAO() {
		ArrayList<ModelVendas> listamodelVendas = new ArrayList();
		ModelVendas modelVendas = new ModelVendas();
		try {
			this.conectar();
			this.executarSQL("SELECT codigo, valor_total, data_venda, desconto, codigo_usuario, gorjeta  FROM vendas" + ";");

			while (this.getResultSet().next()) {
				modelVendas = new ModelVendas();
				modelVendas.setCodigo(this.getResultSet().getInt(1));
				modelVendas.setValorTotal(this.getResultSet().getBigDecimal(2));
				modelVendas.setDataVenda(this.getResultSet().getDate(3));
				modelVendas.setValorDesconto(this.getResultSet().getBigDecimal(4));
				modelVendas.setCodigoUsuario(this.getResultSet().getInt(5));
				modelVendas.setValorGorjeta(this.getResultSet().getBigDecimal(6));
				listamodelVendas.add(modelVendas);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return listamodelVendas;
	}

	public ArrayList<ModelVendas> getListaVendasDAO(int pCodigo) {
		ArrayList<ModelVendas> listamodelVendas = new ArrayList();
		ModelVendas modelVendas = new ModelVendas();
		try {
			this.conectar();
			this.executarSQL("SELECT " + "codigo_produto," + "codigo_venda," + "quantidade, "
					+ " FROM " + " vendas_produto WHERE codigo_venda = '" + pCodigo + "'" + ";");

			while (this.getResultSet().next()) {
				modelVendas = new ModelVendas();
				modelVendas.setProdutosCodigo(this.getResultSet().getInt(1));
				modelVendas.setCodigo(this.getResultSet().getInt(2));
				modelVendas.setQuantidade(this.getResultSet().getFloat(3));
				listamodelVendas.add(modelVendas);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return listamodelVendas;
	}

	public ArrayList<ModelVendas> getListaPedidosDAO(ModelVendas pModelVendas) {
		ArrayList<ModelVendas> listamodelVendas = new ArrayList();
		ModelVendas modelVendas = new ModelVendas();
		try {
			this.conectar();
			this.executarSQL("SELECT codigo, valor_total, data_venda, desconto, codigo_usuario, gorjeta  FROM vendas WHERE (data_venda BETWEEN  '" + pModelVendas.getDataVenda() + "' AND '"
					+ pModelVendas.getDataVenda() + "');");

			while (this.getResultSet().next()) {
				modelVendas = new ModelVendas();
				modelVendas.setCodigo(this.getResultSet().getInt(1));
				modelVendas.setValorTotal(this.getResultSet().getBigDecimal(2));
				modelVendas.setDataVenda(this.getResultSet().getDate(3));
				modelVendas.setValorDesconto(this.getResultSet().getBigDecimal(4));
				modelVendas.setCodigoUsuario(this.getResultSet().getInt(5));
				modelVendas.setValorGorjeta(this.getResultSet().getBigDecimal(6));
				listamodelVendas.add(modelVendas);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return listamodelVendas;
	}

	public ArrayList<ModelVendas> getListaOrcamentoDAO(ModelVendas pModelVendas) {
		ArrayList<ModelVendas> listamodelVendas = new ArrayList();
		ModelVendas modelVendas = new ModelVendas();
		try {
			this.conectar();
			this.executarSQL("SELECT codigo, valor_total, data_venda, desconto, codigo_usuario, gorjeta  FROM vendas WHERE data_venda = '" + pModelVendas.getDataVenda() + "' AND tipo = 0" + ";");

			while (this.getResultSet().next()) {
				modelVendas = new ModelVendas();
				modelVendas.setCodigo(this.getResultSet().getInt(1));
				modelVendas.setValorTotal(this.getResultSet().getBigDecimal(2));
				modelVendas.setDataVenda(this.getResultSet().getDate(3));
				modelVendas.setValorDesconto(this.getResultSet().getBigDecimal(4));
				modelVendas.setCodigoUsuario(this.getResultSet().getInt(5));
				modelVendas.setValorGorjeta(this.getResultSet().getBigDecimal(6));
				listamodelVendas.add(modelVendas);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return listamodelVendas;
	}

	public boolean atualizarVendasDAO(ModelVendas pModelVendas) {
		try {
			this.conectar();
			this.executarUpdateDeleteSQL(
					"UPDATE vendas SET " 
							+ "codigo = '" + pModelVendas.getCodigo() + "'," 
							+ "valor_total = '" + pModelVendas.getValorTotal() + "'," 
							+ "data_venda = '" 	+ pModelVendas.getDataVenda() + "',"
							+ "desconto = '" + pModelVendas.getValorDesconto()+ "'," 
							+ "codigo_usuario = '" + pModelVendas.getCodigoUsuario() + "'," 
							+ "gorjeta = '" + pModelVendas.getValorGorjeta() + "'"
							+ " WHERE " + "codigo = '" + pModelVendas.getCodigo() + "'" + ";");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			this.fecharConexao();
		}
	}

	public boolean excluirVendasDAO(int pCodigo) {
		try {
			this.conectar();
			return this.executarUpdateDeleteSQL("DELETE FROM vendas " + " WHERE " + "codigo = '" + pCodigo + "'" + ";");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			this.fecharConexao();
		}
	}

	public boolean excluirProdutoVendasDAO(int pCodigo) {
		try {
			this.conectar();
			this.executarUpdateDeleteSQL(
					"DELETE FROM vendas_produto WHERE " + "codigo_venda = '" + pCodigo + "'" + ";");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			this.fecharConexao();
		}
	}

	public ArrayList<ModelVendas> getListaVendasProdutosDAO(int pCodigo) {
		ArrayList<ModelVendas> listamodelVendas = new ArrayList();
		ModelVendas modelVendas = new ModelVendas();
		try {
			this.conectar();
			this.executarSQL("SELECT " + "codigo_produto," + "codigo_venda," + "quantidade " + " FROM "
					+ " vendas_produto WHERE codigo_venda = '" + pCodigo + "'" + ";");

			while (this.getResultSet().next()) {
				modelVendas = new ModelVendas();
				modelVendas.setProdutosCodigo(this.getResultSet().getInt(1));
				modelVendas.setCodigo(this.getResultSet().getInt(2));
				modelVendas.setQuantidade(this.getResultSet().getFloat(3));
				listamodelVendas.add(modelVendas);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return listamodelVendas;
	}

}
