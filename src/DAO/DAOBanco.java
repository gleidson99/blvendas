package DAO;

import conexoes.ConexaoMySql;
import model.ModelSessaoUsuario;

public class DAOBanco extends ConexaoMySql {

    public boolean criarBancoDAO() {
        try {
            this.conectarSemBanco();
            
            this.executarUpdateDeleteSQL("CREATE DATABASE " + ModelSessaoUsuario.nomeDoBanco);
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            this.fecharConexao();
        }
        return true;
    }

}
