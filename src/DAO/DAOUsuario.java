package DAO;

import java.util.ArrayList;

import conexoes.ConexaoMySql;
import model.ModelUsuario;

public class DAOUsuario extends ConexaoMySql {

	public int salvarUsuarioDAO(ModelUsuario pModelUsuario) {
		try {
			this.conectar();
			return this.insertSQL("INSERT INTO usuarios (" + "nome," + "login," + "senha," + "senha_cancelamento_item" + ") VALUES (" + "'" + pModelUsuario.getNome() + "'," + "'" + pModelUsuario.getLogin() + "'," + "'" + pModelUsuario.getSenha()
					+ "'," + "'" + pModelUsuario.getSenhaCancelamento() + "'" + ");");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			this.fecharConexao();
		}
	}

	public ModelUsuario getUsuarioDAO(int pCodigo) {
		ModelUsuario modelUsuario = new ModelUsuario();
		try {
			this.conectar();
			this.executarSQL("SELECT " + "pk_codigo," + "nome," + "login," + "senha," + "senha_cancelamento_item" + " FROM" + " usuarios" + " WHERE" + " pk_codigo = '" + pCodigo + "'" + ";");

			while (this.getResultSet().next()) {
				modelUsuario.setCodigo(this.getResultSet().getInt(1));
				modelUsuario.setNome(this.getResultSet().getString(2));
				modelUsuario.setLogin(this.getResultSet().getString(3));
				modelUsuario.setSenha(this.getResultSet().getString(4));
				modelUsuario.setSenhaCancelamento(this.getResultSet().getString(5));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return modelUsuario;
	}

	public ModelUsuario getUsuarioDAO(String pLogin) {
		ModelUsuario modelUsuario = new ModelUsuario();
		try {
			this.conectar();
			this.executarSQL("SELECT " + "pk_codigo," + "nome," + "login," + "senha," + "senha_cancelamento_item" + " FROM" + " usuarios" + " WHERE" + " login = '" + pLogin + "'" + ";");

			while (this.getResultSet().next()) {
				modelUsuario.setCodigo(this.getResultSet().getInt(1));
				modelUsuario.setNome(this.getResultSet().getString(2));
				modelUsuario.setLogin(this.getResultSet().getString(3));
				modelUsuario.setSenha(this.getResultSet().getString(4));
				modelUsuario.setSenhaCancelamento(this.getResultSet().getString(5));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return modelUsuario;
	}

	public boolean getUsuarioDAO(ModelUsuario pModelUsuario) {
		try {
			this.conectar();
			this.executarSQL(
					"SELECT " + "pk_codigo," + "nome," + "login," + "senha," + "senha_cancelamento_item" + " FROM" + " usuarios" + " WHERE" + " login = '" + pModelUsuario.getLogin() + "' AND senha = '" + pModelUsuario.getSenha() + "' " + ";");

			if (getResultSet().next()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			this.fecharConexao();
		}
	}

	public ArrayList<ModelUsuario> getListaUsuarioDAO() {
		ArrayList<ModelUsuario> listamodelUsuario = new ArrayList();
		ModelUsuario modelUsuario = new ModelUsuario();
		try {
			this.conectar();
			this.executarSQL("SELECT " + "pk_codigo," + "nome," + "login," + "senha," + "senha_cancelamento_item" + " FROM" + " usuarios" + ";");

			while (this.getResultSet().next()) {
				modelUsuario = new ModelUsuario();
				modelUsuario.setCodigo(this.getResultSet().getInt(1));
				modelUsuario.setNome(this.getResultSet().getString(2));
				modelUsuario.setLogin(this.getResultSet().getString(3));
				modelUsuario.setSenha(this.getResultSet().getString(4));
				modelUsuario.setSenhaCancelamento(this.getResultSet().getString(5));
				listamodelUsuario.add(modelUsuario);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.fecharConexao();
		}
		return listamodelUsuario;
	}

	public boolean atualizarUsuarioDAO(ModelUsuario pModelUsuario) {
		try {
			this.conectar();
			return this.executarUpdateDeleteSQL("UPDATE usuarios SET " + "pk_codigo = '" + pModelUsuario.getCodigo() + "'," + "nome = '" + pModelUsuario.getNome() + "'," + "login = '" + pModelUsuario.getLogin() + "'," + "senha = '"
					+ pModelUsuario.getSenha() + "'," + "senha_cancelamento_item = '" + pModelUsuario.getSenhaCancelamento() + "'" + " WHERE " + "pk_codigo = '" + pModelUsuario.getCodigo() + "'" + ";");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			this.fecharConexao();
		}
	}

	public boolean excluirUsuarioDAO(int pCodigo) {
		try {
			this.conectar();
			return this.executarUpdateDeleteSQL("DELETE FROM usuarios " + " WHERE " + "pk_codigo = '" + pCodigo + "'" + ";");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			this.fecharConexao();
		}
	}
}