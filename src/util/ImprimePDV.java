package util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.JOptionPane;

import controller.ControllerItensPedidoMesa;
import controller.ControllerProdutos;
import controller.ControllerVendas;
import model.ModelItensPedidoMesa;
import model.ModelVendas;

public class ImprimePDV {

	ControllerItensPedidoMesa controllerItensPedidoMesa = new ControllerItensPedidoMesa();
	ControllerProdutos controllerProdutos = new ControllerProdutos();
	ControllerVendas controllerVendas = new ControllerVendas();
	ArrayList<ModelItensPedidoMesa> listaModelintensPedidoMesa = new ArrayList<>();
	ModelVendas modelVendas = new ModelVendas();
	BLMascaras bLMascaras = new BLMascaras();

	public void geraCupomTXT(int pCodigo) throws IOException {
		modelVendas = controllerVendas.getVendasController(pCodigo);
		listaModelintensPedidoMesa = controllerItensPedidoMesa.getListaItensPedidoPorVenda(pCodigo);
		// data e hora do sistema
		String data = "dd/MM/yyyy";
		String hora = "hh:mm";
		String hora1;
		Date agora = new java.util.Date();
		SimpleDateFormat formata = new SimpleDateFormat(data);
		formata = new SimpleDateFormat(hora);
		hora1 = formata.format(agora);
		// fim data e hora do sistema

		String conteudoFor = "";
		for (int i = 0; i < listaModelintensPedidoMesa.size(); i++) {
			conteudoFor += listaModelintensPedidoMesa.get(i).getQuantidade() + " x "
					+ controllerProdutos.getProdutosController(listaModelintensPedidoMesa.get(i).getCodigoProduto())
							.getValor()
					+ " - " + controllerProdutos
							.getProdutosController(listaModelintensPedidoMesa.get(i).getCodigoProduto()).getDescricao()
					+ "\n\r";
		}
		String textoParaImprimir = ("Petisqueira Padre Rolim\n\r"
				+ "R. Padre Rolim - 159\n\r"
				+"Santa Efigênia\n\r"
				+"Belo Horizonte - MG\n\r" 
				+ "Data: " + formata.format(modelVendas.getDataVenda()) + "-" + hora1 + "\n\r"
				+ "--------------------------------\n\r" + "        CUPOM NAO FISCAL        \n\r"
				+ "--------------------------------\n\r" + "QT   PRECO   DESCRICAO\n\r" + conteudoFor + ""
				+ "--------------------------------\n\r" + "Sub Total: " + modelVendas.getValorTotal() + "\n\r"
				+ "   Desconto: " + modelVendas.getValorDesconto() + "\n\r"
				+ "   Gorjetas: " + modelVendas.getValorGorjeta() + "\n\r"
				+ "Valor total: " + modelVendas.getValorTotal().subtract(modelVendas.getValorDesconto()).add(modelVendas.getValorGorjeta()) + "\n\r"
				+ "\n\r\n\r\n\r\n\r\f");
		System.out.println(textoParaImprimir);
		this.imprimir(textoParaImprimir);

	}

	public void geraCupomCozinha(int pCodigo) throws IOException {

		modelVendas = controllerVendas.getVendasController(pCodigo);
		listaModelintensPedidoMesa = controllerItensPedidoMesa.getListaItensPedidoPorVenda(pCodigo);
		// data e hora do sistema
		String data = "dd/MM/yyyy";
		String hora = "h:mm - a";
		String data1, hora1;
		java.util.Date agora = new java.util.Date();
		SimpleDateFormat formata = new SimpleDateFormat(data);
		data1 = formata.format(agora);
		formata = new SimpleDateFormat(hora);
		hora1 = formata.format(agora);
		// fim data e hora do sistema

		String conteudoFor = "";
		for (int i = 0; i < listaModelintensPedidoMesa.size(); i++) {
			conteudoFor += listaModelintensPedidoMesa.get(i).getQuantidade()
					+ " x " + controllerProdutos
							.getProdutosController(listaModelintensPedidoMesa.get(i).getCodigoProduto()).getDescricao()
					+ " - " + listaModelintensPedidoMesa.get(i).getCodigoVenda() + "\n\r";
		}
		// armazenar em uma string o texto(cupom) para imprimir
		String textoParaImprimir = ("Petisqueira Padre Rolim\n\r"
				+ "R. Padre Rolim - 159\n\r"
				+"Santa Efigênia\n\r"
				+"Belo Horizonte - MG\n\r" + "Data venda: "
				+ modelVendas.getDataVenda() + "\n\r" + "Impressao:" + data1 + "-" + hora1 + "\n\r"
				+ "--------------------------------\n\r" + "        CUPOM COZINHA        \n\r"
				+ "--------------------------------\n\r" + "QT   DESCRICAO   OBS.\n\r" + conteudoFor + ""
				+ "--------------------------------\n\r" + "Valor bruto: " + modelVendas.getValorTotal() + "\n\r"
				+ "   Desconto: " + modelVendas.getValorDesconto() + "\n\r" + "Valor total: " + modelVendas.getValorTotal().subtract(modelVendas.getValorDesconto()) + "\n\r" + "\n\r\n\r\n\r\n\r\f");
		// chamar metodo para imprimir
		// /n/r para novas linhas e /f para fim da pagina
		this.imprimir(textoParaImprimir);

	}

	public void geraCupomDelivery(int pCodigo) throws IOException {
		modelVendas = controllerVendas.getVendasController(pCodigo);
		listaModelintensPedidoMesa = controllerItensPedidoMesa.getListaItensPedidoPorVenda(pCodigo);
		// modelClienteCidadeEstado = controllerClienteCidadeEstado.getClienteCidadeEstadoController(modelVendas.getClientesCodigo());

		// data e hora do sistema
		String data = "dd/MM/yyyy";
		String hora = "h:mm - a";
		String data1, hora1;
		java.util.Date agora = new java.util.Date();
		SimpleDateFormat formata = new SimpleDateFormat(data);
		data1 = formata.format(agora);
		formata = new SimpleDateFormat(hora);
		hora1 = formata.format(agora);
		// fim data e hora do sistema

		String conteudoFor = "";
		for (int i = 0; i < listaModelintensPedidoMesa.size(); i++) {
			conteudoFor += listaModelintensPedidoMesa.get(i).getQuantidade()
					+ " x " + controllerProdutos
							.getProdutosController(listaModelintensPedidoMesa.get(i).getCodigoProduto()).getDescricao()
					+ " - " + listaModelintensPedidoMesa.get(i).getCodigoVenda() + "\n\r";
		}
		// armazenar em uma string o texto(cupom) para imprimir
		String textoParaImprimir = ("Petisqueira Padre Rolim\n\r"
				+ "R. Padre Rolim - 159\n\r"
				+"Santa Efigênia\n\r"
				+"Belo Horizonte - MG\n\r"  + "Data venda: "
				+ modelVendas.getDataVenda() + "\n\r" + "Impressao:" + data1 + "-" + hora1 + "\n\r"
				+ "--------------------------------\n\r" + "         CUPOM DELIVERY         \n\r"
				+ "--------------------------------\n\r" + "             CLIENTE            \n\r"
				+ "--------------------------------\n\r"
				+ "--------------------------------\n\r" + "QT   DESCRICAO   OBS.\n\r" + conteudoFor + ""
				+ "--------------------------------\n\r" + "Valor bruto: " + modelVendas.getValorTotal() + "\n\r"
				+ "   Desconto: " + modelVendas.getValorDesconto() + "\n\r" + "Valor total: " + modelVendas.getValorTotal().subtract(modelVendas.getValorDesconto()) + "\n\r" + "\n\r\n\r\n\r\n\r\f");
		// chamar metodo para imprimir
		// /n/r para novas linhas e /f para fim da pagina
		this.imprimir(textoParaImprimir);

	}

	public void imprimir(String pTexto) {

		try {
			InputStream prin = new ByteArrayInputStream(pTexto.getBytes());
			DocFlavor docFlavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
			SimpleDoc documentoTexto = new SimpleDoc(prin, docFlavor, null);
			PrintService impressora = PrintServiceLookup.lookupDefaultPrintService();
			// pega a //impressora padrao
			PrintRequestAttributeSet printerAttributes = new HashPrintRequestAttributeSet();
			printerAttributes.add(new JobName("Impressao", null));
			printerAttributes.add(OrientationRequested.PORTRAIT);
			printerAttributes.add(MediaSizeName.ISO_A4);
			// informa o tipo de folha
			DocPrintJob printJob = impressora.createPrintJob();

			try {
				// tenta imprimir
				printJob.print(documentoTexto, (PrintRequestAttributeSet) printerAttributes);
			} catch (PrintException e) {
				// mostra //mensagem de erro
				JOptionPane.showMessageDialog(null, "Não foi possível realizar a impressão !!", "Erro",
						JOptionPane.ERROR_MESSAGE);
			}

			prin.close();

		} catch (Exception e) {

		}

	}

}
