package util;

import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.swing.JOptionPane;

import model.ModelSessaoUsuario;

public class BackupMySQL {

    ModelSessaoUsuario modelSessaoUsuario = new ModelSessaoUsuario();

    public boolean Backupdbtosql() {

        try {
            String folderPath = "C:\\BLVendas\\backup";
            File f1 = new File(folderPath);
            f1.mkdir();

            String savePath = "C:\\BLVendas\\backup\\" + "backup_" + new Date().getTime() + ".sql";
            String executeCmd = modelSessaoUsuario.caminhoMySQL +"mysqldump.exe --user=" + 
                    modelSessaoUsuario.usuarioBanco + 
                    " --password=" + modelSessaoUsuario.senhaBanco + 
                    " --database " + modelSessaoUsuario.nomeDoBanco + " -r " + savePath;
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
            if (processComplete == 0) {
                System.out.println("Backup Complete");
                return true;
            } else {
                System.out.println("Backup Failure");
                return false;
            }

        } catch (IOException | InterruptedException ex) {
            JOptionPane.showMessageDialog(null, "Error at Backuprestore" + ex.getMessage());
            return false;
        }
    }

    public boolean Restoredbfromsql(String pCaminho) {
        try {
            String[] executeCmd = new String[]{modelSessaoUsuario.caminhoMySQL+"mysql.exe", modelSessaoUsuario.nomeDoBanco, "--user=" + modelSessaoUsuario.usuarioBanco, "--password=" + modelSessaoUsuario.senhaBanco, "-e", " source " + pCaminho};
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
            if (processComplete == 0) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Error at restoring");
                return false;
            }
        } catch (IOException | InterruptedException | HeadlessException ex) {
            JOptionPane.showMessageDialog(null, "Error at Restoredbfromsql" + ex.getMessage());
            return false;
        }
    }

}
